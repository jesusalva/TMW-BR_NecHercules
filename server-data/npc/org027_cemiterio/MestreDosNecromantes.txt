

org027,126,80,0	script	Hadet	NPC575,{
    if (getskilllv(SKILL_NECROMANTE) == 0 && (QUEST_Classe < 2 || BaseLevel < 40)) goto L_SemNivel;
    if (getskilllv(SKILL_NECROMANTE) == 0 && QUEST_Classe == 3) goto L_Retorno;
    if (getskilllv(SKILL_NECROMANTE) == 0 && QUEST_Classe == 4) goto L_JaDoutraClasse;
    if (getskilllv(SKILL_NECROMANTE) >= 1) goto L_JaDestaClasse;
    goto L_IniciaQuest;

L_JaDoutraClasse:
    mes "[Hadet]";
    mes "\"Agora você já pertence a classe...\"";

    @Classe = SKILL_CAVALEIRO;
    callfunc "GerarNomeDaPatente";
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;

    @Classe = SKILL_DRUIDA;
    callfunc "GerarNomeDaPatente";
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;

    @Classe = SKILL_MAGO;
    callfunc "GerarNomeDaPatente";
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;

    @Classe = SKILL_NINJA;
    callfunc "GerarNomeDaPatente";
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;

    @Classe = SKILL_PALADINO;
    callfunc "GerarNomeDaPatente";
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;

    @Classe = SKILL_NECROMANTE;
    callfunc "GerarNomeDaPatente";
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;
    close;

L_JaDestaClasse:
    @Classe = SKILL_NECROMANTE;
    callfunc "GerarNomeDaPatente";
    mes "[Hadet]";
    mes "\"Meus parabéns! Agora você é um" + @a$ + " " + @NomeDaPatente$ + "!\"";
    close;

L_SemNivel:
    mes "[Hadet]";
    mes "\"Olá viajante.\"";
    close;

L_IniciaQuest:
    mes "Você vê um homem com um manto preto, emamando uma aura sombria.";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "(^_^) \"O senhor é o Mestre dos Necromantes?\"";
    next;
    mes "[Hadet]";
    mes "(^_^) \"Sou. Meu nome é Hadet, Mestre dos Necromantes do Mundo de Mana.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "(^_^) \"O Mestre Hadet poderia me ensinar a ser um" + @a$ + " Necromante?\"";
    next;
    mes "[Hadet]";
    mes "(^_^) \"Pode ser que sim, pode ser que não...";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
    "(·_·) Por favor, belo mestre! O senhor gostará de minha ajuda!", L_Falha,
    "(T.T) Se não me aceitar, digo que o senhor colocou meleca do nariz no copo da princesa!", L_Falha,
    "(X.x) Preciso ser aceito ou levarei uma surra de minha mamãe!", L_Falha,
    "(-_-) Posso prová-lo que serei um excelente aprendiz!", L_Provar,
    "(^_^) Faço qualquer coisa se me aceitar! Exceto aos domingos, claro!", L_Falha,
    "(^_^) Tenho certeza um homem tão bacana como o senhor irá me aceitar! ", L_Falha,
    "(¬.¬) Não preciso de sua ajuda. Eu nem queria mesmo!", L_Falha;

L_Falha:
    mes "";
    mes "[Hadet]";
    mes "(T_T) \"Saia daqui antes que eu chame meus escravos infernais!\"";
    close;

L_Provar:
    mes "";
    mes "[Hadet]";
    mes "(^_^) \"Isso me interessa...\"";
    next;
    mes "[Hadet]";
    mes "(T_T) \"Mas para provar seu valor, preciso que você me traga alguns itens.\"";
    next;
    mes "[Hadet]";
    mes "(¬.¬) \"Eles são: 50 [" + getitemlink("AntenaLuminosa") + "] e 30 [" + getitemlink("AntenaRosa") + "].\"";
    next;
    mes "[Hadet]";
    mes "(T_T) \"Mas antes saiba que se você escolher ser da classe dos Necromantes não poderá aprender outra classe depois. Também é preciso saber se você é realmente excepcional, e se sabe o que está fazendo.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
    "(^-^) Como o senhor quiser! Eu já trago o que me foi ordenado.", L_Trago,
    "(T.T) Ahh, esquece! Desisto desta porcaria!", L_Fechar;

L_Trago:
    QUEST_Classe = 3;
    mes "";
    mes "[Hadet]";
    mes "(¬.¬) \"Boa sorte mortal.\"";
    close;

L_Retorno:
    mes "[Hadet]";
    mes "\"(T_T) Trouxe os itens que ordenei?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
    "(^-^) Sim, aqui estão.", L_Pronto,
    "(X.x) Não, Esqueci quais são!", L_Esqueci;

L_Pronto:
    if (countitem("AntenaLuminosa") < 50 || countitem("AntenaRosa") < 30) goto L_Esqueci;
    getinventorylist;
    if (@inventorylist_count >= 100) goto L_SemLugar;
    delitem "AntenaLuminosa", 50;
    delitem "AntenaRosa", 30;
    getitem "GrimoriumNegro", 1;
    addtoskill SKILL_NECROMANTE, 1;
    QUEST_Classe = 4;
    mes "";
    mes "[Hadet]";
    mes "(^_^) \"Parabéns mortal. Você se mostrou capaz de fazer o que eu ordeno. Agora você é um aprendiz dos Necromantes.\"";
    next;
    mes "[Hadet]";
    mes "(^_^) \"Eu te dou de presente esse Grimorium Negro que é o livro para você anotar os segredos guia dos Necromantes.\"";
    close;

L_Esqueci:
    mes "";
    mes "[Hadet]";
    mes "(¬_¬) \"Bem, você não trouxe o que eu te pedi.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "(X.x) \"Opa! Acho que perdi alguns itens por aeee.\"";
    next;
    mes "[Hadet]";
    mes "(T_T) \"Não esqueça que os itens são esses!";
    mes " * 50 [" + getitemlink("AntenaLuminosa") + "].";
    mes " * 30 [" + getitemlink("AntenaRosa") + "].\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "(X.x) \"Senhor! Sim senhor! Não esquecerei senhor! Ja volto senhor!\"";
    close;

L_SemLugar:
    mes "[Hadet]";
    mes "\"Sua bolsa está bem cheia, não é? Libere espaço e depois volte para pegar sua recompensa.\"";
    close;

L_Fechar:
    close;
}
