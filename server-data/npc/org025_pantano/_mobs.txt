org025,100,60,56,79	monster	Trevos	1037,3,30000,10000,Moborg025::On1037
org025,56,96,13,5	monster	Trevos	1037,1,1200000,10000,Moborg025::On1037
org025,56,36,77,33	monster	Planta Alizarina	1032,2,240000,12000,Moborg025::On1032
org025,55,62,24,13	monster	Tufo	1020,7,30000,12000,Moborg025::On1020
org025,38,84,37,31	monster	Bicho da Seda	1035,18,40000,300,Moborg025::On1035
org025,96,47,63,55	monster	Tronco Vivo	1025,14,18000,3000,Moborg025::On1025
org025,73,61,106,83	monster	Flor	1014,34,20000,5000,Moborg025::On1014
org025,73,61,107,82	monster	Cogumelo	1019,40,90000,500,Moborg025::On1019
org025,101,53,55,65	monster	Esquilo	1038,18,60000,9000,Moborg025::On1038
org025,26,61,15,85	monster	Escorpião	1003,12,100000,50000,Moborg025::On1003


org025,0,0,0	script	Moborg025	NPC32767,{
    end;

On1003:
    @mobId = 1003;
    callfunc "MobPoints";
    callsub S_MOBS_escorpiao;
    callsub S_MOBS_QueimEscorp;
    end;

On1014:
    @mobId = 1014;
    callfunc "MobPoints";
    end;

On1019:
    @mobId = 1019;
    callfunc "MobPoints";
    end;

On1020:
    @mobId = 1020;
    callfunc "MobPoints";
    end;

On1025:
    @mobId = 1025;
    callfunc "MobPoints";
    callsub S_MOBS_cordaPescador;
    callsub S_MOBS_madeiraResist;
    callsub S_MOBS_escudoMadeira;
    callsub S_MOBS_pedraAguia;
    callfunc "drop_ambar_pedraAguia";
    end;

On1032:
    @mobId = 1032;
    callfunc "MobPoints";
    end;

On1035:
    @mobId = 1035;
    callfunc "MobPoints";
    end;

On1037:
    @mobId = 1037;
    callfunc "MobPoints";
    end;

On1038:
    @mobId = 1038;
    callfunc "MobPoints";
    end;



S_MOBS_cordaPescador:
    if (QUEST_cordaPescador != 1) goto L_Return;
    @max = 30;
    @mobs = MOBS_cordaPescador;
    @flag = MOBS_cordaPescador;
    callfunc "mobContagem";
    MOBS_cordaPescador = @mobs;
    @MOBS_cordaPescador = @flag;
    return;

S_MOBS_escorpiao:
    if (QUEST_praia != 12) goto L_Return;
    @max = 30;
    @mobs = QUEST_contamobs;
    @flag = @QUEST_contamobs;
    callfunc "mobContagem";
    QUEST_contamobs = @mobs;
    @QUEST_contamobs = @flag;
    return;

S_MOBS_escudoMadeira:
    if (QUEST_escudoMadeira != 1) goto L_Return;
    @max = 130;
    @mobs = MOBS_escudoMadeira;
    @flag = @MOBS_escudoMadeira;
    callfunc "mobContagem";
    MOBS_escudoMadeira = @mobs;
    @MOBS_escudoMadeira = @flag;
    return;

S_MOBS_madeiraResist:
    if (QUEST_madeiraResist != 1) goto L_Return;
    @max = 100;
    @mobs = MOBS_madeiraResist;
    @flag = @MOBS_madeiraResist;
    callfunc "mobContagem";
    MOBS_madeiraResist = @mobs;
    @MOBS_madeiraResist = @flag;
    return;

S_MOBS_pedraAguia:
    if (QUEST_pedraAguia != 1) goto L_Return;
    @max = 100;
    @mobs = MOBS_pedraAguia;
    @flag = @MOBS_pedraAguia;
    callfunc "mobContagem";
    MOBS_pedraAguia = @mobs;
    @MOBS_pedraAguia = @flag;
    return;

S_MOBS_QueimEscorp:
    if (QUEST_pocaoQueimadura != 2) goto L_Return;
    @max = 20;
    @mobs = MOBS_queimaduraEscorpiao;
    @flag = @MOBS_queimaduraEscorpiao;
    callfunc "mobContagem";
    MOBS_queimaduraEscorpiao = @mobs;
    @MOBS_queimaduraEscorpiao = @flag;
    return;

L_Return:
    return;
}
