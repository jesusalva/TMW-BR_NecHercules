

005,102,72,0	script	Anselmo	NPC315,{
    goto L_Inicio;

L_Inicio:
    @SexNumeral$ = "um";
    @SexCacador$ = "Caçador";
    if (Sex == 0) goto L_config_garota;
    goto L_Falas;

L_config_garota:
    @SexNumeral$ = "uma";
    @SexCacador$ = "Caçadora";
    goto L_Falas;

L_Falas:
    if (SeCacadorDeNecromantes == 1) goto L_ja_cacador;
    if (BaseLevel >= 40) goto L_nivel_minimo;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Eu estou procurando trabalho.",            L_Tchau,
        "Você sabe onde posso encontrar trabalho?", L_Tchau;

L_Tchau:
    mes "";
    mes "[Anselmo]";
    mes "\"Eu não tenho tempo para gente fraca feito você!\"";
    close;

L_nivel_minimo:
    if (SeCacadorDeNecromantes == 1) goto L_ja_cacador;
    mes "[Anselmo]";
    mes "\"O rei me mandou contratar Caçadores de Criminosos.";
    mes "Você quer esse trabalho, " + strcharinfo(0) + "?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Sim, sim eu quero!",                  L_Sim,
        "Estou precisando muito de trabalho.", L_Sim,
        "Me fale mais sobre o assunto!",       L_mais_sobre,
        "Não, obrigad" + @fm$ + ".",           L_Nao2;

L_Nao2:
    mes "";
    mes "[Anselmo]";
    mes "\"Então, até mais. Covarde!\"";
    close;

L_ja_cacador:
    mes "[" + strcharinfo(0) + "]";
    menu
        "Quero ver novamente a lista de criminosos!", L_Lista2,
        "Não, queixa quieto!",                        L_Tchau2;

L_Tchau2:
    mes "";
    mes "[Anselmo]";
    mes "Certo! Volte quando for delatar o nome de um criminoso!";
    close;

L_Sim:
    mes "";
    mes "[Anselmo]";
    mes "\"Certo " + strcharinfo(0) + "!";
    mes "Assine seu nome abaixo!\"";
    @n = 0;
    goto L_assinar_nome;

L_assinar_nome:
    @n = @n + 1;
    input @NomeDigitado$;
    if (@NomeDigitado$ == "") goto L_Desistir_de_assinar;
    if (@NomeDigitado$ == strcharinfo(0)) goto L_assinado_certo;
    if (@n == 3) goto L_impressaoDigital;
    mes "[Anselmo]";
    mes "\"Céus! Você não sabe nem escrever seu nome corretamente?\"";
    next;
    mes "[Anselmo]";
    mes "\"Digite seu nome corretamente diferenciando maiúsculas das minúsculas!\"";
    next;
    mes "[Anselmo]";
    mes "\"Assine seu nome abaixo novamente!\"";
    goto L_assinar_nome;

L_impressaoDigital:
    mes "";
    mes "[Anselmo]";
    mes "\"Quanta dificuldade pra assinar o próprio nome!";
    mes "Vou ter que me contentar com sua digital apenas... coloque seu polegar aqui.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Isso eu sei fazer!",          L_assinado_certo,
        "Só colocar aqui?",            L_assinado_certo,
        "Não quero mais seu emprego!", L_Desistir_de_assinar2;

L_assinado_certo:
    mes "";
    mes "[Anselmo]";
    mes "\"Pronto " + strcharinfo(0) + "!";
    mes "Você é oficialmente " + @SexNumeral$ + " " + @SexCacador$ + " de Criminosos!";
    mes "Boa sorte!\"";
    SeCacadorDeNecromantes = 1;
    close;

L_Desistir_de_assinar:
    mes "";
    mes "[" + strcharinfo(0) + "]";
    mes "\"Não vou mais assinar! Não quero mais seu emprego!\"";
    next;
    goto L_Desistir_de_assinar2;

L_Desistir_de_assinar2:
    mes "";
    mes "[Anselmo]";
    mes "\"Está certo!\"";
    next;
    mes "[Anselmo]";
    mes "\"Então, até mais. Covarde!\"";
    close;

L_mais_sobre:
    mes "";
    mes "[Anselmo]";
    mes "\"Existem algum criminoso espalhando pelo mundo.";
    mes "O trabalho de " + @SexNumeral$ + " " + @SexCacador$ + " de Criminosos é encontrá-los e denunciá-los.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Mas quais são os tipos de criminoso?", L_Lista,
        "Que criminosos são estes?",            L_Lista;

L_Lista:
    mes "";
    mes "[Anselmo]";
    mes "\"Deixa eu te dar uma lista.\"";
    next;
    goto L_Lista2;

L_Lista2:
    mes "[LISTA]";
    menu
        "* Rebeldes de lingua suja;",    L_rebeldes,
        "* Viciados em Mendigancia;",    L_viciados,
        "* Falsarios;",                  L_falcarios,
        "* Trombadinhas;",               L_trombadinhas,
        "* Praticantes de necromancia;", L_necromantes,
        "Já conheço todos!",             L_nivel_minimo;

L_rebeldes:
    mes "";
    mes "[Anselmo]";
    mes "\"\"Rebeldes de Lingua Suja\" são todos ateles que ofendem outros cidadão. Aqui é crime grave e pode dar até prisão perpétua na cadeia por chingar alguém. Mesmo que alguém te chingou primeiro.\"";
    next;
    mes "[Anselmo]";
    mes "\"Para caçar um \"Rebeldes de Lingua Suja\" você só precisa postar uma screenshot da ofença no tópico \"Bugs/Reclamações\" do forum \"www.themanaworld.com.br\". Então, os Generais se encarregaram de punir o criminoso.\"";
    next;
    goto L_Lista2;

L_viciados:
    mes "";
    mes "[Anselmo]";
    mes "\"\"Viciados em Mendigancia\" são aqueles que são aqueles que incomodar outros cidadãos com insistentes pedidos.\"";
    next;
    mes "[Anselmo]";
    mes "\"Para caçar um \"Viciados em Mendigancia\" você só precisa postar uma screenshot da insistencia no tópico \"Bugs/Reclamações\" do forum \"www.themanaworld.com.br\". Então, os Generais se encarregaram de punir o criminoso.\"";
    next;
    goto L_Lista2;

L_falcarios:
    mes "";
    mes "[Anselmo]";
    mes "\"\"Falsarios\" ou \"Fakes\" são aqueles que se disfarçam de garotas para fazer amizades com os cidadão e receber itens como presentes.\"";
    next;
    mes "[Anselmo]";
    mes "\"Para caçar um \"Fake\" você só precisa postar uma screenshot com a prova da falcidade no tópico \"Bugs/Reclamações\" do forum \"www.themanaworld.com.br\". Então, os Generais se encarregaram de punir o criminoso.\"";
    next;
    goto L_Lista2;

L_trombadinhas:
    mes "";
    mes "[Anselmo]";
    mes "\"\"Trombadinhas\" são aqueles que se passam por seus amigos para te pedir emprestado um item e nunca mais devolvem.\"";
    next;
    mes "[Anselmo]";
    mes "\"Para caçar um \"Trombadinhas\" você só precisa postar uma screenshot com a prova do acordo detalhado de emprestimo no tópico \"Bugs/Reclamações\" do forum \"www.themanaworld.com.br\". Então, os Generais se encarregaram ou não de punir o criminoso.\"";
    next;
    mes "[Anselmo]";
    mes "\"O ideal é que não impreste nada, a menos que sua intensão seja de presentear o que se está emprestando!\"";
    next;
    goto L_Lista2;

L_necromantes:
    mes "";
    mes "[Anselmo]";
    mes "\"Não sei muito sobre \"Necromantes\". Só sei que eles fazem esperiencias proibidas com os corpos dos cidadão mortos.\"";
    next;
    mes "[Anselmo]";
    mes "\"Se você encontar algum \"Necromante\", venha denunciá-lo para mim! Ok?\"";
    next;
    goto L_Lista2;
}

005,111,73,0	script	#Anselmo	NPC32767,{
    if (BaseLevel < 40 || SeCacadorDeNecromantes == 1) end;
    @n = rand(3);
    if (Sex == 1 && @n == 0) npctalk strnpcinfo(0), "Estou contratando corajoso jovem para ser um \"Caçador de Criminosos\"!";
    if (Sex == 1 && @n == 1) npctalk strnpcinfo(0), "##3"+ strcharinfo(0) +"##0, você que ser um \"Caçador de Criminosos\"?";
    if (Sex == 1 && @n == 2) npctalk strnpcinfo(0), "O Rei convoca \"Caçadores de Criminosos\"!";
    if (Sex == 0 && @n == 0) npctalk strnpcinfo(0), "Estou contratando corajosa jovem para ser uma \"Caçadora de Criminosos\"!";
    if (Sex == 0 && @n == 1) npctalk strnpcinfo(0), "##3"+ strcharinfo(0) +"##0, você que ser uma \"Caçadora de Criminosos\"?";
    if (Sex == 0 && @n == 2) npctalk strnpcinfo(0), "O Rei convoca \"Caçadores de Criminosos\"!";
    end;
}
