005,166,63,0	script	Ismael	NPC102,{
    if (MPQUEST == 0) goto L_Register;
    mes "[Ismael (Guia de Caça)]";
    mes "Você tem " + Mobpt + " Pontos de Monstro. Estes pontos foram adquiridos ao matar monstros.";
    close;

L_Register:
    mes "[Ismael (Guia de Caça)]";
    mes "Oh meu, você não parece ser registrado como um caçador participante. Você gostaria de se registrar?";
    next;
    goto L_Choice;

L_Choice:
    menu
        "Sim, quero me registrar.",    L_R,
        "Não no momento.",             L_N,
        "Mais informações por favor.", L_I;

L_R:
    mes "";
    mes "[Ismael (Guia de Caça)]";
    mes "Dê me um segundo para olhar a sua ficha.";
    next;
    mes "[Ismael (Guia de Caça)]";
    mes "Bem, pacere que você é qualificado!";
    mes "Seja bem-vind" + @fm$ + " ao mundo da caçada!";
    MPQUEST = 1;
    close;

L_N:
    mes "";
    mes "[Ismael (Guia de Caça)]";
    mes "Muito bem, você não sabe o que está perdendo.";
    close;

L_I:
    mes "";
    mes "[Ismael (Guia de Caça)]";
    mes "Aqui no The Mana World BR, existem algumas vantagens quando você vence seus inimigos.";
    mes "Por exemplo, os Pontos de Monstro. Cada monstro que você matar tem uma certa quantidade de pontos que serão adicionados à sua conta.";
    mes "Quanto mais pontos você tiver, mais itens poderão ser trocados por estes pontos.";
    next;
    mes "[Ismael (Guia de Caça)]";
    mes "Você vai se registrar, não vai?";
    next;
    goto L_Choice;
}
