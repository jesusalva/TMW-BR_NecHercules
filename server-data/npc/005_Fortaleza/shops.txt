005,92,67,0	shop	Jorge	NPC120,1199:3,529:5,1200:1000,530:3000
005,105,55,0	shop	Abelardo	NPC112,513:15,534:45,501:25,533:150
005,109,70,0	shop	Sofia	NPC139,519:100,534:45,501:25,533:150
005,92,61,0	shop	Geraldo	NPC159,539:87,562:575,676:100
005,92,72,0	shop	Lara	NPC103,501:50,502:70,567:500,568:500,750:450,684:250,685:500
005,108,66,0	shop	Ferraz	NPC135,625:20000,626:50000,658:100000,1201:25,522:100,521:1000
005,96,55,0	shop	Oscar	NPC142,535:25,657:40
005,98,60,0	shop	#Jogador LVL 99	NPC32767,3378:1000000

005,98,60,0	script	Jogador LVL 99	NPC120,{
    if (BaseLevel < 99) goto L_Fraco;
    goto L_Poderoso;

L_Poderoso:
    npctalk strnpcinfo(0), "Tenho mesmo algo para os jogadores poderosos como você!";
    goto L_Shop;
    
L_Fraco:
    @n = rand(2);
    if (@n == 0) npctalk strnpcinfo(0), "Hmm... você não parece forte o bastante para aquilo que eu vendo.";
    if (@n == 1) npctalk strnpcinfo(0), "Sinto muito, você é fraco demais para o meu boné.";
    goto L_Shop;

L_Shop:
    shop "#Jogador LVL 99";
}
