hali001,48,21,0	script	Fernando	NPC147,7,7,{
    if ($PosseDeHalicarnazo != 0 && Reino != $PosseDeHalicarnazo) goto L_SugestaoAoInvasor;
    if (QUEST_Fernando == 0 && BaseLevel < 50) goto L_SemLevel;
    if (QUEST_Fernando == 1) goto L_Retorno;
    if (QUEST_Fernando == 2) goto L_Fim;
    goto L_IniciaQuest;

L_SugestaoAoInvasor:
    heal -(MaxHp / 10), 0;
    @n = rand(4);
    if (@n == 0) npctalk strnpcinfo(0), "Saia daqui seu invasor de Halicarnazo!";
    if (@n == 1) npctalk strnpcinfo(0), "Você não pode entrar em Halicarnazo!";
    if (@n == 2) npctalk strnpcinfo(0), "Como você se atreve a tentar machucar as pessoas do Reino de Halicarnazo?";
    if (@n == 3) npctalk strnpcinfo(0), "Eu te mostrarei do que o povo de Halicarnazo é feito.";
    end;

L_SemLevel:
    emotion EMOTE_SAD;
    npctalk strnpcinfo(0), "Estou tão preocupado com minha esposa. Pena que não vejo ninguem com bravura o suficinete para me ajudar...";
    end;

L_IniciaQuest:
    mes "[" + strcharinfo(0) + "]";
    mes "\"Olá senhor! Estou procurando uma boa arma, o que você quer de mim por alguma de suas armas?\"";
    next;
    mes "[Fernando]";
    mes "\"Ohhh, finalmente alguém apareceu! Por favor, me ajude guerreiro!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Ajudar em quê??", L_Explicar,
        "Este cara está louco t.t", L_Fechar;

L_Explicar:
    mes "[Fernando]";
    mes "\"Desculpe não me apresentar, eu se chamo Fernando! Acabei de receber a informação de que minha mulher foi enfeitiçada por uma fada necromante no Reino de Lilit!\"";
    next;
    mes "[Fernando]";
    mes "\"Minha mulher foi enfeitiçada por uma malvada necromante. Ela estava em Lilit quando viu uma fada Necromante e foi enfeitaçada.\"";
    next;
    mes "[Fernando]";
    mes "\"A fada disse a ela que se não trouxesse 40 Asas de Fada e 20 Pós Encantados ela ficaria amaldiçoada para sempre!!!\"";
    next;
    mes "[Fernando]";
    mes "\"Será que podia me ajudar a tirar a maldição de minha mulher pegando 40 Asas de Fada e 20 Pós Encantados?\"";
    next;
    mes "\"Como recompensa posso te dar a melhor machadinha que tenho aqui em minha loja. Ela foi usada por um bravo guerreiro chamado Tomas Hawk que morreu quarenta anos atrás, antes de morrer, me deixou esta bela Machadinha que diz ser abençoada!\"";
    mes "\"Eu faço qualquer coisa, mas por favor ajude minha mulher!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Está bem, eu te ajudo, mas vou querer a machadinha de Tomas Hawk.", L_Quest,
        "Não tenho tempo para te ajudar ferreiro!", L_Fechar;

L_Quest:
    mes "[Fernando]";
    mes "\"Ok, mas seja o mas rápido possivel pois a fada deu um prazo de 7 dias para trazer os itens senão a maldição iria ser permanente!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Não se preocupe senhor, deixa comigo que em 7 dias sua esposa estará salva!\"";
    next;
    mes "[Fernando]";
    mes "\"Ok, mas lembre-se, são 40 [" + getitemlink("AsasDeFada") + "] e 20 [" + getitemlink("BocadoDePoEncantado") + "].\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Ok!\"";
    QUEST_Fernando = 1;
    close;

L_Retorno:
    mes "[Fernando]";
    mes "\"Trouxe as 40 Asas de Fada e os 20 Pós Encantados?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Sim, aqui estão.", L_Pronto,
        "Não trouxe, Fernando.", L_Nao2Trouxe;

L_Pronto:
    getinventorylist;
    if (countitem("AsasDeFada") < 40 || countitem("BocadoDePoEncantado") < 20) goto L_Nao2tem;
    if (@inventorylist_count >= 100) goto L_SemLugar;
    delitem "AsasDeFada", 40;
    delitem "BocadoDePoEncantado", 20;
    getitem "MachadinhaThomasHawk", 1;
    QUEST_Fernando = 2;
    mes "[Fernando]";
    mes "\"Aqui está sua Machadinha como recompensa!\"";
    next;
    mes "[Fernando]";
    mes "\"Obrigado pelos itens, agora minha querida esposinha poderá ser desencantada!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"De nada senhor, qualquer dia volto aqui em sua loja.\"";
    mes "\"Obrigado pela Machadinha!\"";
    close;

L_Nao2Trouxe:
    mes "[Fernando]";
    mes "\"Você não trouxe o que pedi!\"";
    next;
    mes "\"Lembre-se que são 40 [" + getitemlink("AsasDeFada") + "] e 20 [" + getitemlink("BocadoDePoEncantado") + "].\"";
    close;

L_Nao2tem:
    mes "[Fernando]";
    mes "\"Você não tem os itens que pedi! Pensa que me enganar! Hahaha.\"";
    next;
    mes "\"Volte e pegue os itens, lembrando que são 40 [" + getitemlink("AsasDeFada") + "] e 20 [" + getitemlink("BocadoDePoEncantado") + "].\"";
    close;

L_Fim:
    mes "[Fernando]";
    mes "\"Olá " + strcharinfo(0) + ", que bom vê-lo de novo!\"";
    close;

L_SemLugar:
    mes "\"Sinto muito, mas... você está com o inventário cheio!\"";
    goto L_Fechar;

L_Fechar:
    close;

OnTouch:
	if ($PosseDeHalicarnazo != 0 && Reino != $PosseDeHalicarnazo) goto L_Raiva;
	if (BaseLevel < 50 || QUEST_Fernando >= 2) end;
    emotion EMOTE_QUEST;
    end;

L_Raiva:
    emotion EMOTE_UPSET;
    heal -(MaxHp / 10), 0;
    end;
}
