hali001,45,58,0	shop	#Arqueiro	NPC32767,1199:3,529:5,1200:1000,530:3000
hali001,74,96,0	shop	Bibliotecário	NPC120,3207:350000,3208:350000,3209:350000,3210:350000,3211:350000,3212:350000,4058:10
hali001,114,51,0	shop	Catchup	NPC112,539:87,676:200,562:500,513:15,534:45
hali001,82,21,0	shop	Feiticeira	NPC103,567:500,568:500,684:250,685:500
hali001,70,133,0	shop	Garçonete#2	NPC139,503:75
hali001,171,121,0	shop	#Larissa	NPC32767,1202:2500,586:2500,632:5000,724:5000,735:5000,741:10000,688:15000,723:50000,564:60000,624:70000,720:80000,3048:90000,3267:100000,3222:50000,3101:200000,3035:100000
hali001,103,132,0	shop	Mostarda	NPC112,539:87,676:200,562:500,513:15,534:45
hali001,158,118,0	shop	#Vendedor	NPC32767,3378:1000000


hali001,45,58,0	script	Arqueiro	NPC120,{
    @n = rand(3);
    if (@n == 0) npctalk strnpcinfo(0), "Temos arcos e flechas de qualidade!!";
    if (@n == 1) npctalk strnpcinfo(0), "Comprem, comprem!!";
    if (@n == 2) goto L_PrecisaDeFlechas;
    goto L_ShopArqueiro;

L_ShopArqueiro:
    shop "#Arqueiro";

L_PrecisaDeFlechas:
    npctalk strnpcinfo(0), "Precisa de flechas?";
    emotion EMOTE_HAPPY;
    goto L_ShopArqueiro;
}

hali001,171,121,0	script	Larissa	NPC301,{
    npctalk strnpcinfo(0), "Só roupas de primeira qualidade aqui!";
    shop "#Larissa";
}

hali001,158,118,0	script	Vendedor	NPC120,{
    if (BaseLevel < 99) goto L_Fraco;
    goto L_Poderoso;

L_Poderoso:
    npctalk strnpcinfo(0), "Tenho mesmo algo para os jogadores poderosos como você!";
    goto L_Shop;
    
L_Fraco:
    @n = rand(2);
    if (@n == 0) npctalk strnpcinfo(0), "Hmm... você não parece forte o bastante para aquilo que eu vendo.";
    if (@n == 1) npctalk strnpcinfo(0), "Sinto muito, você é fraco demais para o meu boné.";
    goto L_Shop;

L_Shop:
    shop "#Vendedor";
}
