007-4,101,64,0	script	Oráculo	NPC324,{
    set @Custo, 300;    
    if (Zeny < 300) goto L_SemGrana;

    if (Sex == 0) set @obrigado$, "obrigada";
    if (Sex == 1) set @obrigado$, "obrigado";

    mes "[Oráculo]";
    mes "=) \"Você deseja que eu diga o seu futuro?\"";
    next;
    mes "[Oráculo]";
    mes "=) \"Vai lhe custar apenas " + @Custo + " GP.\"";
    next;
    menu
        "Sim, eu quero!", L_pagar,
        "Não, " + @obrigado$ + ".", L_Tchau;

L_Tchau:
    emotion EMOTE_ANGEL;
    mes "";
    mes "[Oráculo]";
    mes "=D \"Sei que, cedo ou tarde, você irá procurar minha consultoria mística.\"";
    close;

L_pagar:
    if (Zeny < @Custo) goto L_SemGrana;
    set Zeny, Zeny - @Custo;    
    goto L_adivinhar;

L_adivinhar:
    @ContPrevisoes = 0;
    mes "";

    emotion EMOTE_HAPPY;
    mes "[Oráculo]";
    mes "=D \"Bom! Aqui vou eu...\"";
    next;

    mes "[Oráculo]";
    mes "\"Em sua história de vida, você está no capitulo "+CAP+"-"+SUBCAP+".";
    if (CAP == 0 && SUBCAP == 0) mes "Você deveria conversar com a Enfermeira.\"";
    if (CAP == 1 && SUBCAP == 1) mes "A enfermeira te recomendou dois personagens sendo que apenas um poderá lhe ajudar.\"";
    //if (CAP == 1 && SUBCAP == 2) mes "Você teve ideias sobre quem você é, mas deveria procurar pessoas com equipamentos fortes junto aos pescadores.";

    // Caso você tenha completado o máximo possível de quests da história principal...
    if (CAP >= 1 && SUBCAP >= 2) mes "Você teve ideias sobre quem você é, e deveria refletir sobre elas.\"";
    mes "";
        
    if (QUEST_bauCamelia >= 0 && QUEST_bauCamelia <= 10) emotion EMOTE_ANGEL;
    if (QUEST_bauCamelia >= 0 && QUEST_bauCamelia <= 10) set @ContPrevisoes, @ContPrevisoes + 1;
    if (QUEST_bauCamelia >= 0 && QUEST_bauCamelia <= 10) mes "[Oráculo]";
    if (QUEST_bauCamelia == 0) mes "\"A enfermeira da vila dos pescadores vai lhe dar uma chave estranha de baú que contem algumas de suas coisas perdidas.\"";
    if (QUEST_bauCamelia >= 1 && QUEST_bauCamelia <= 10) mes "\"Você encontrou a enfermeira da vila dos pescadores. Ela está procurando suas coisas perdidas.\"";
    if (QUEST_bauCamelia >= 0 && QUEST_bauCamelia <= 10) next;

    //mes "[Oráculo]";
    //mes "\"Você vai encontrar uma mulher casada em uma das casas da vila dos pescadores. Ela vai estar muito preocupada com a prima dela.\"";
    //next;

    if (QUEST_praia >= 0 && QUEST_praia <= 13) emotion EMOTE_ANGEL;
    if (QUEST_praia >= 0 && QUEST_praia <= 13) set @ContPrevisoes, @ContPrevisoes + 1;
    if (QUEST_praia >= 0 && QUEST_praia <= 13) mes "[Oráculo]";
    if (QUEST_praia == 0) mes "\"Você encontrará numa das casas da vila dos pescadores uma mulher que está muito preocupada com a prima dela.\"";
    if (QUEST_praia >= 1 && QUEST_praia <= 7) mes "\"Você está ajudando numa das casas da vila dos pescadores uma mulher casada com um pescador apaixonado.\"";
    if (QUEST_praia >= 8 && QUEST_praia <= 9) mes "\"Você encontrará um pescador em pé sobre o dique da vila dos pescadores que tem a memória fraca.\"";
    if (QUEST_praia >= 10 && QUEST_praia <= 13) mes "\"Você está ajudando o pescador de memória fraca.\"";
    if (QUEST_praia >= 0 && QUEST_praia <= 13) next; // QUEST_praia >= 14 → NPC concluido!

    if (Gismo_capitulo >= 0 && Gismo_capitulo <= 1) emotion EMOTE_ANGEL;
    if (Gismo_capitulo >= 0 && Gismo_capitulo <= 1) set @ContPrevisoes, @ContPrevisoes + 1;
    if (Gismo_capitulo >= 0 && Gismo_capitulo <= 1) mes "[Oráculo]";
    if (Sex == 1 && Gismo_capitulo == 0) mes "\"Você será extorquido por um homem na vila dos pescadores que tem informações importantes sobre como encontrar um necromante.\"";
    if (Sex == 1 && Gismo_capitulo == 1) mes "\"Você está sendo extorquido por um homem que precisa muita madeira.\"";
    if (Sex == 0 && Gismo_capitulo == 0) mes "\"Você será galanteada exageradamente por um homem na vila dos pescadores que tem informações importantes sobre como encontrar um necromantes.\"";
    if (Sex == 0 && Gismo_capitulo == 1) mes "\"Você está sendo extorquida e galanteada exageradamente por um homem que precisa muita madeira.\"";
    if (Gismo_capitulo >= 0 && Gismo_capitulo <= 1) next; // Gismo_capitulo >= 2 → NPC concluido!

    //mes "[Oráculo]";
    //mes "\"Você vai encontrar um fazendeiro que está com problemas de praga nas plantações. Cedo ou tarde você topará trabalhar para ele. Pois ele tem um ferramenta que lhe será bem útil.\"";
    //next;

    //mes "[Oráculo]";
    //mes "\"Em feira de Bhramir, você vai encontrar um bêbado que tem um gosto estranho por comida. Ele vai querer negociar os petiscos favoritos com você. E, ao meu ver irá lhe pagar razoavelmente bem!\"";
    //next;

    if (getskilllv(1) <= 0) emotion EMOTE_ANGEL;
    if (getskilllv(1) <= 0) set @ContPrevisoes, @ContPrevisoes + 1;
    if (getskilllv(1) <= 0) mes "[Oráculo]";
    if (getskilllv(1) <= 0) mes "\"Você encontrará uma mulher na vila dos pescadores que gosta de fazer caretas. Ela te ensinará a fazer caretas.\"";
    if (getskilllv(1) <= 0) next; //getskilllv(1) >= 1 → Ja tem a habilidade de expressar emoticons

    if (getskilllv(2) <= 0) emotion EMOTE_ANGEL;
    if (getskilllv(2) <= 0) set @ContPrevisoes, @ContPrevisoes + 1;
    if (getskilllv(2) <= 0) mes "[Oráculo]";
    if (getskilllv(2) <= 0) mes "\"Você encontrará um comerciante na vila dos pescadores que te ensinará como você pode comercializar com outros cidadãos.\"";
    if (getskilllv(2) <= 0) next; // getskilllv(1) >= 1 → Ja tem a habilidade de negociar com outros jogadores
goto L_Fim;

L_SemGrana:
    emotion EMOTE_TONGUE;
    mes "[Oráculo]";
    mes "=P \"Ja sei que você não tem grana suficiente em seus bolsos para pagar por minha consultoria mística.\"";
    close;

L_Fim:
    emotion EMOTE_DISGUST;
    mes "[Oráculo]";
    mes "Xþ \"Huuuuuuuuuuum!\"";
    next;
    emotion EMOTE_SURPRISE;
    mes "*Pufff!*";
    next;
    emotion EMOTE_BLUSH;
    mes "[Oráculo]";
    mes "=S \"Me perdoe! Não tenho mais nenhuma previsão.\"";
    next;
    if (@ContPrevisoes <= 0) emotion EMOTE_SAD;
    if (@ContPrevisoes <= 0) set Zeny, Zeny + @Custo;
    if (@ContPrevisoes <= 0) mes "[Oráculo]";
    if (@ContPrevisoes <= 0) mes "=( \"Aqui está sua grana de volta!\"";
    if (@ContPrevisoes <= 0) close;
    mes "[Oráculo]";
    mes "=) \"Deseja que eu repita o que eu falei? Não cobro nada para repetir.\"";
    next;
    menu
        "Sim, por favor.", L_adivinhar,
        "Não, " + @obrigado$ + "!",  L_Tchau2;

L_Tchau2:
    emotion EMOTE_GRIN;
    mes "";
    mes "[Oráculo]";
    mes "=D \"Ok! Espero ter ajudado.\"";
    close;
}
