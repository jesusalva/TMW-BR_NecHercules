




007-4,97,26,0	script	Camélia	NPC119,1,1,{
    if (Sex == 0) set @meu_minha$, l("minha");
    if (Sex == 1) set @meu_minha$, l("meu");
    if (Sex == 0) set @obrigado$, l("Obrigada");
    if (Sex == 1) set @obrigado$, l("Obrigado");

    if (QUEST_MASK1 & MASK1_CAMELIA_BAU) goto L_MenuFim;
    if (QUEST_bauCamelia == 0) goto L_Inicio;
    if (QUEST_bauCamelia == 1) goto L_procuraEquipamento;
    if (QUEST_bauCamelia == 2) goto L_abraBau;
    if (QUEST_bauCamelia == 3) goto L_abriuBau;
    if (QUEST_bauCamelia == 5) goto L_procuraEquipamento2;
    if (QUEST_bauCamelia == 6) goto L_abraBau;
    if (QUEST_bauCamelia == 7) goto L_devolveFaca;
    if (QUEST_bauCamelia == 9 && gettimetick(2) >= QUEST_bauCameliaTime + 60 * 60) goto L_chave3;
    if (QUEST_bauCamelia == 10) goto L_abraBau;

    mes l("[Enfermeira Camélia]");
    mesq l("Olá @@ jovem. O que posso fazer por você?", @meu_minha$);
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        l("Você poderia me curar?"), L_Curar,
        l("Nada. @@.", @obrigado$), L_Fechar;


L_Inicio:
    mes l("[Enfermeira Camélia]");
    mesq l("Olá @@ jovem, que bom que acordou! Já estava ficando preocupada, pois já fazia mais de 24 horas que você estava em um sono profundo...", @meu_minha$);
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        l("Fazia?! E como você sabe disso?"),   L_comoPararAqui,
        l("Ai... minha cabeça ainda dói."),     L_comRazao,
        l("Você poderia me curar?"),            L_Curar;

L_comoPararAqui:
    mes l("[Enfermeira Camélia]");
    mes l("\"Você não se lembra?!");
    goto L_Explicacao;

L_comRazao:
    mes l("[Enfermeira Camélia]");
    mes l("\"Com razão!");
    goto L_Explicacao;

L_Explicacao:
    mes l("Você foi encontrado gravemente ferido, precisando de cuidados médicos, com apenas uma Faca e roupas de algodão.\"");
    next;
    mes "";
    mes l("[Enfermeira Camélia]");
    mesq l("Você é muito forte sabia?! E teve muita sorte de ter sobrevivido.");
    next;
    mes "["+ strcharinfo(0) +"]";
    mesq l("A única coisa que eu me lembro é que eu estava caçando quando levei uma forte pancada na cabeça. Antes disto não me lembro de mais nada!");
    next;
    mes l("[Enfermeira Camélia]");
    mesq l("Então, na realidade você foi encontrado aqui, e como não tinham leitos te enviamos em segredo para cidade na qual você parecia ter nascido.");
    next;
    mes "["+ strcharinfo(0) +"]";
    mesq l("Você sabe se eu possuía alguma coisa comigo?");
    next;
    mes l("[Enfermeira Camélia]");
    mesq l("Você foi trazido para este hospital apenas com a roupa do corpo e uma faca antes de ser encaminhado para seu reino... e mais nada. Provavelmente você foi saqueado, pois é pouco provável alguém ir caçar com roupas de algodão e uma faca! É loucura! Se é que você me entende.");
    next;
    mes "["+ strcharinfo(0) +"]";
    mesq l("Entendo!");
    next;
    mes "["+ strcharinfo(0) +"]";
    mesq l("Mesmo assim, preciso encontrar mais alguma coisa que lembre o meu passado!");
    next;
    mes l("[Enfermeira Camélia]");
    mesq l("Eu vou perguntar sobre isso aos pescadores que te trouxeram.");
    next;
    mes "["+ strcharinfo(0) +"]";
    mesq l("Está certo! Enquanto isso vou dar uma volta para tentar lembrar de algo...");
    QUEST_bauCamelia = 1;
    QUEST_bauCameliaTime = gettimetick(2);
    close;


L_procuraEquipamento:
    mes "";
    mes l("[Enfermeira Camélia]");
    mesq l("Olá @@ jovem, que bom que voltou!", @meu_minha$);
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        l("Você encontrou algo meu?"), L_calcular1,
        l("Você poderia me curar novamente?"), L_Curar,
        l("Só passei para cumprimentar..."),   L_Tchau;

L_Tchau:
    mes "";
    mes l("[Enfermeira Camélia]");
    mesq l("Ok! Então, tchau!");
    close;

L_calcular1:
    if (gettimetick(2) >= QUEST_bauCameliaTime + 3 * 60) goto L_chave1;
    mes "";
    mes l("[Enfermeira Camélia]");
    mes l("\"Tenha calma, pois eu ainda irei falar com os pescadores daqui @@ minuto(s).", (((QUEST_bauCameliaTime + 3 * 60) - gettimetick(2)) / 60 + 1 ));
    mes l("Eu prometo.\"");
    close;

L_chave1:
    mes "";
    mes l("[Enfermeira Camélia]");
    mesq l("Eu falei com os pescadores, mas eles disseram que não encontraram muita coisa no local. Só esta @@.", getitemlink("FacaAfiada"));
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        l("Posso ver a faca?"),        L_ganhaChave1,
        l("A faca não me interessa."), L_Fechar;

L_ganhaChave1:
    mes "";
    mes l("[Enfermeira Camélia]");
    mes l("\"Claro, eu guardei ela em meu baú.");
    mes l("Você poderia pegar esta chave e abrir meu baú, por favor?\"");
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        l("Sim, pode deixar que eu abro."), L_Abrir;

L_Abrir:
    getinventorylist;
    if (@inventorylist_count == 100 && countitem(3022) == 0) goto L_lotado;
    getitem 3022, 1;
    QUEST_bauCamelia = 2;
    close;


L_abraBau:
    mes l("[Enfermeira Camélia]");
    mesq l("Abra o baú com a chave que te dei, por favor!");
    close;

L_abriuBau:
    mes "";
    mes l("[Enfermeira Camélia]");
    mesq l("Você encontrou a faca, @@ jovem?", @meu_minha$);
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        l("Sim, mas acho que ela não é minha."), L_verdadeFaca,
        l("Sim, é essa mesma!"),                 L_mentiraFaca;

L_verdadeFaca:
    mes "";
    mes l("[Enfermeira Camélia]");
    mesq l("Não é! Vou comentar isso com os pescadores.");
    next;
    mes l("[Enfermeira Camélia]");
    mesq l("Por enquanto fique com ela, já que não tem dono.");
    QUEST_bauCamelia = 5;
    QUEST_bauCameliaTime = gettimetick(2);
    goto L_Continuacao2;

L_mentiraFaca:
    mes "";
    mes l("[Enfermeira Camélia]");
    mesq l("Que bom que encontrou parte de seu equipamento.");
    QUEST_bauCamelia = 4;
    goto L_Fechar;


L_procuraEquipamento2:
    mes "";
    mes l("[Enfermeira Camélia]");
    mesq l("Olá @@ jovem, que bom que voltou!", @meu_minha$);
    next;
    goto L_Continuacao2;

L_Continuacao2:
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        l("Você encontrou mais alguma coisa?"), L_calcular2,
        l("Você poderia me curar?"),            L_Curar,
        l("Te vejo depois..."),                 L_Fechar;

L_calcular2:
    if (gettimetick(2) >= QUEST_bauCameliaTime + 10 * 60)
        goto L_chave2;
    mes "";
    mes l("[Enfermeira Camélia]");
    mes l("\"Tenha calma, pois eu ainda irei falar com os pescadores daqui @@ minuto(s).", (((QUEST_bauCameliaTime + 10 * 60) - gettimetick(2)) / 60 + 1));
    mes l("Volte mais tarde.\"");
    close;

L_chave2:
    mes "";
    mes l("[Enfermeira Camélia]");
    mesq l("Um pescador encontrou algo perto de onde você foi encontrado.");
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        l("Posso ver o que foi encontrado?"),  L_ganhaChave2,
        l("Depois eu passo aqui para ver..."), L_Tchau2;
 
L_Tchau2:
    mes "";
    mes l("[Enfermeira Camélia]");
    mesq l("Ok! Então até logo.");
    close;

L_ganhaChave2:
    mes "";
    mes l("[Enfermeira Camélia]");
    mes l("\"Sim, você pode.");
    mes l("E aqui está a chave. Você já sabe o que fazer...\"");
    getinventorylist;
    if (@inventorylist_count==100 && countitem(3022)==0) goto L_lotado;
    getitem 3022, 1;
    QUEST_bauCamelia = 6;
    close;


L_devolveFaca:
    mes "";
    mes l("[Enfermeira Camélia]");
    mesq l("Então... você encontrou o que procurava no baú?");
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        l("Sim... eu me lembro dessa Adaga."), L_Sim;

L_Sim:
    mes "";
    mes l("[Enfermeira Camélia]");
    mesq l("Por acaso você ainda está com a @@ que te dei?", getitemlink("FacaAfiada"));
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        l("Sim. E quero devolver."), L_devolver,
        l("Não. Eu já a vendi."),    L_vendeu,
        l("Você poderia me curar?"), L_Curar;

L_vendeu:
    mes "";
    mes l("[Enfermeira Camélia]");
    mesq l("Desta vez você me decepcionou. Como pode vender algo que não era seu?");
    QUEST_bauCamelia = 8;
    close;

L_devolver:
    if (countitem(522) == 0) goto L_semFaca;
    mes "";
    mes l("[Enfermeira Camélia]");
    mes l("\"Você me surpreende! Nem precisei pedir que devolvesse aquela faca.");
    mes l("Vou continuar a perguntar sobre seu equipamento. Tenha a certeza disso.\"");
    delitem 522, 1;
    QUEST_bauCameliaTime = gettimetick(2);
    QUEST_bauCamelia = 9;
    close;

L_semFaca:
    mes "";
    mes l("[Enfermeira Camélia]");
    mesq l("Eu acho que você perdeu a faca que te dei.");
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        l("Sim! Me perdoe. Eu não tive a intenção!"), L_Perdi;

L_Perdi:
    mes "";
    mes l("[Enfermeira Camélia]");
    mesq l("Tudo bem!");
    close;


L_chave3:
    mes "";
    mes l("[Enfermeira Camélia]");
    mesq l("Eu tenho duas notícias para você. Uma boa e uma não tão boa.");
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        l("Comece pela notícia boa."),         L_NoticiaNaoBoa,
        l("Comece pela notícia não tão boa."), L_NoticiaNaoBoa; 

L_NoticiaNaoBoa:
    mes "";
    mes l("[Enfermeira Camélia]");
    mes l("\"Vou começar pela não tão boa:");
    mes l("Os pescadores voltaram aqui e me avisaram que mais nada foi encontrado.");
    mes l("Sem esperanças de encontrar o resto do seu equipamento.\"");
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "E a notícia boa.", L_NoticiaBoa;

L_NoticiaBoa:
    mes "";
    mes l("[Enfermeira Camélia]");
    mes l("\"A notícia boa é que eles foram solidários e lhe mandaram um presente.");
    mes l("Pegue a chave de meu baú e descubra o que tem lá.\"");
    getinventorylist;
    if (@inventorylist_count == 100 && countitem(3022) == 0) goto L_lotado;
    getitem 3022, 1;
    QUEST_bauCamelia = 10;
    QUEST_bauCameliaTime = 0;
    close;


L_Curar:
    if (BaseLevel > 20) goto L_temPagar;
    mes "";
    mes l("[Enfermeira Camélia]");
    mes l("\"Você realmente não me parece bem.");
    mes l("Mas essa agulhada vai resolver!\"");
    next;
    @grito = rand(3);
    if (@grito == 0) message strcharinfo(0), l("Ai!");
    if (@grito == 1) message strcharinfo(0), l("Eu não gosto de injeção. Ui!");
    if (@grito == 2) message strcharinfo(0), l("Ai! Ai! Ai! Ai! Ai! ");
    if (@grito == 3) message strcharinfo(0), l("Vai dueeeeeeeeer.... O.o!");
    heal (MaxHp), 0;
    close;

L_temPagar:
    mes "";
    mes l("[Enfermeira Camélia]");
    mes l("\"Sim, eu posso. Mas você terá que me ajudar também.");
    mes l("Esta injeção é cara e exitem outras pessoas mais necessitadas.");
    mes l("Mas eu poderia te curar por 500 GP.\"");
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        l("Tudo bem... eu pago."), L_pagarOk,
        l("Não mesmo!"),           L_Fechar;

L_pagarOk:
    if (Zeny < 500) goto L_SemGrana;
    Zeny = Zeny - 500;
    mes "";
    mes l("[Enfermeira Camélia]");
    mes "\"Tudo bem então. Vou aplicar a injeção.\"";
    close2;
    @grito = rand(4);
    if (@grito == 0) message strcharinfo(0), l("Ai!");
    if (@grito == 1) message strcharinfo(0), l("Eu não gosto de injeção. Ui!");
    if (@grito == 2) message strcharinfo(0), l("Ai! Ai! Ai! Ai! Ai! ");
    if (@grito == 3) message strcharinfo(0), l("Vai dueeeeeeeeer.... O.o!");
    heal (MaxHp), 0;
    end;

L_lotado:
    mes "";
    mes l("[Enfermeira Camélia]");
    mesq l("Você não tem espaço para minha chave. Volte mais tarde.");
    close;

L_SemGrana:
    mes "";
    mes l("[Enfermeira Camélia]");
    mesq l("Você não tem dinheiro o suficiente para pagar pela injeção.");
    close;

L_MenuFim:
    mes l("[Enfermeira Camélia]");
    mesq l("\"Olá @@ jovem. O que posso fazer por você?", @meu_minha$);
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        l("Você poderia me curar?"),                      L_Curar,
        l("Você descobriu mais alguma coisa sobre mim?"), L_sobreVoce,
        l("Nada. @@.", @obrigado$),          L_Fechar;

L_sobreVoce:
    // Se você ainda não começou a história principal, atualiza a sua conta.
    if (CAP < 1) goto L_iniciaMainQuest;

    goto L_sobreVoce2;

L_sobreVoce2:
    mes "";
    mes l("[Enfermeira Camélia]");
    mesq l("Infelizmente, você é um completo mistério para todos nós. Nenhuma pessoa desta vila te conhece ou sabe de onde você veio.");
    next;
    mesq l("Eu te aconselho a explorar o máximo que puder, conversar com as pessoas, conhecer novas cidades... Talvez esta seja a chave para recuperar sua memória perdida.");
    if (QUEST_MASK1 & MASK1_MONGE || Q11_alquimista >= 2) close;    // Q11 => Q(uest) 1(cap) 1(sub)
    next;
    mesq l("Hmm... Uma outra dica é pedir alguns conselhos para o monge... ele é um senhor muito sábio que fica meditando nos arredores da ilha de fortaleza. Ele vive falando sobre auto-conhecimento... talvez ele possa te ajudar.");
    mes "";
    mesq l("Mas caso você não acredite em monges, tinha um alquimista muito esperto, que foi seu discípulo no passado. Talvez ele possa te ajudar também.");
    close;

L_iniciaMainQuest:
    // Se você vendeu a faca afiada, não possui espirito de um embaixador.
    if (QUEST_bauCamelia == 8) set PATH_EMBAIXADOR, -1;

    // Se você mentiu acerca da faca afiada, com certeza não é um herói.
    if (QUEST_bauCamelia == 5) set PATH_HEROI, -1;

    // Se você fez tudo certinho, dá um bônus para todas as classes (ser honesto é sempre bom).
    if (QUEST_bauCamelia == 10) set PATH_HEROI, 1;
    if (QUEST_bauCamelia == 10) set PATH_EMBAIXADOR, 1;
    if (QUEST_bauCamelia == 10) set PATH_GENERAL, 1;
    if (QUEST_bauCamelia == 10) set PATH_RECLUSO, 1;

    // Finalmente, te inclui na História Principal, capitulo 1-1.
    CAP = 1;
    SUBCAP = 1;
    getexp 100, 0;

    mes "";
    mes l("[História Principal 1-1]");
    mes l("Você recebeu 100 pontos de experiência.");
    mes l("Você agora está no capitulo 1-1.");
    next;

    goto L_sobreVoce2;

L_Fechar:
    close;

OnTouch:
    if (QUEST_bauCamelia >= 9) end;
    emotion EMOTE_QUEST;
    end;
}
