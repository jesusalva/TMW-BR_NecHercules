

012,157,153,0	script	Coelho Pascal	NPC173,{
    @hatCascaOvo = 1256;
    @orelhasLebre = 1255;
    @bonePascoa = 3224;

    if (PASCOA2_esconde == 0) goto L_Inicio;
    if (PASCOA2_esconde == 1) goto L_voltou;
    if (PASCOA2_esconde == 2) goto L_Retorno1;         // Procurando 10 ovos. 8,33% de chance.
    if (PASCOA2_esconde == 3) goto L_devePremio1;      // Ganha um 'chapéu Casca de Ovo' para pintar.
    if (PASCOA2_esconde == 4) goto L_desafio2;
    if (PASCOA2_esconde == 5) goto L_MenuEsconderOvos; // Escondendo 30 ovos. 16,67% de chance.
    if (PASCOA2_esconde == 6) goto L_CoelhoProcuraOvos;
    if (PASCOA2_esconde == 7) goto L_devePremio2;      // Ganha uma 'orelha de lebre' para pintar.
    if (PASCOA2_esconde == 8) goto L_desafio3;
    if (PASCOA2_esconde == 9) goto L_procuraOvosPrata; // Procurando 10 ovos prata.
    if (PASCOA2_esconde== 10) goto L_devePremio3;      // Ganha um 'Boné orelhas coelho'

    // Caso não seja nenhuma das opções o script é encerrado com um 'Feliz Páscoa'
    mes "[Coelho Pascal]";
    mes "\"Espero que tenha se divertido muito!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Quero fazer um Ovo de Páscoa.",         L_MenuOvo,
        "Quero pintar meu Chapéu Casca de Ovo.", L_MenuCascaOvo,
        "Me diverti muito!!!",                   L_meDiverti;

L_meDiverti:
    mes "";
    mes "[Coelho Pascal]";
    mes "XD \"Feliz Páscoa!\"";
    close;


L_Inicio:
    mes "[Coelho Pascal]";
    mes "\"Olá! Posso te ajudar?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Estou entediado... Existe algo de interessante por aqui?\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"Bem... Um monte de coelhos e lebres! Quer coisa mais interessante do que isso? ;)\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"Brincadeira... Eu entendi o que você quis dizer... Você parece ser uma pessoa que gosta de desafios. Acho que tenho a proposta ideal para você!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"É disso que eu gosto! Qual seria a proposta?\"";
    @next = 1;
    goto L_deNovo;

L_deNovo:
    if (@next == 0) mes "";
    if (@next == 1) next;
    mes "[Coelho Pascal]";
    mes "\"É bem simples! Eu escondi vários ovos de chocolate por todo esse lugar. Mas como eu escondi muito bem, acho bem difícil alguém encontrá-los. Por isso resolvi te dar essa chance de mostrar sua sagacidade. Encontre 10 ovos de chocolate e fale comigo.\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"Isso pode ser difícil, mas é apenas para começar o desafio... Mas não se preocupe, se você for bem eu vou te recompensar muito bem!\"";
    PASCOA2_esconde = 1;
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Ótimo! Mas por onde eu começo a procurar?",               L_Aceitou,
        "Ahh, estou sem paciência agora. Talvez depois eu volte.", L_sair1;

L_voltou:
    mes "[Coelho Pascal]";
    mes "\"Olá! Decidiu voltar para aceitar meu desafio?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Sim! Agora estou pronto. Mas por onde devo começar?",    L_Aceitou,
        "Tenho dúvidas. Pode me explicar de novo?",               L_deNovo,
        "Não, ainda estou sem paciência para esconde-esconde...", L_sair1;

L_sair1:
    mes "";
    mes "[Coelho Pascal]";
    mes "\"Tudo bem! Sinta-se à vontade para voltar quando quiser.\"";
    close;

L_Aceitou:
    mes "";
    mes "[Coelho Pascal]";
    mes "\"Use a sua imaginação! Pode ser em QUALQUER lugar. Dentro de um lago, atrás de uma árvore, no meio de arbustos... qualquer lugar!\"";
    PASCOA2_esconde = 2;
    PASCOA2_escondeCont = 0;
    PASCOA2_escondeNum = rand(12);
    close;

L_Retorno1:
    if (PASCOA2_escondeCont == 0) goto L_nenhumOvo;
    if (PASCOA2_escondeCont >= 10) goto L_encontrouOvos;

    mes "[Coelho Pascal]";
    mes "\"Olá! Como vai a procura? Encontrou alguma coisa?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    if (PASCOA2_escondeCont == 1)
        menu
            "Está muito difícil! Só achei um ate agora!", L_ContinueProcurando;
    if (PASCOA2_escondeCont > 1 && PASCOA2_escondeCont < 3)
        menu
            "Está difícil, mas já achei alguns ovos!", L_ContinueProcurando;
    if (PASCOA2_escondeCont >= 6 && PASCOA2_escondeCont < 7)
        menu
            "Nossa, você escondeu muito bem! Mas não vou desistir!", L_ContinueProcurando;
    //if (PASCOA2_escondeCont >= 10 && PASCOA2_escondeCont < 10)
    menu
        "Hah! Já achei um monte! Estou quase lá!", L_ContinueProcurando;

L_ContinueProcurando:
    mes "";
    mes "[Coelho Pascal]";
    mes "\"Esse é o espírito! Continue procurando...\"";
    close;

L_nenhumOvo:
    mes "[Coelho Pascal]";
    mes "\"Estou vendo que você não encontrou nenhum ovo... Talvez eu tenha superestimado suas habilidades intelectuais... hahaha.\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"Lembre-se de procurar os todos os lugares possíveis. Dentro de um lago, atrás de uma árvore, no meio de arbustos... qualquer lugar!\"";
    close;

L_encontrouOvos:
    mes "[Coelho Pascal]";
    mes "\"UAU!!! Você encontrou mesmo todos os ovos! Estou impressionado...\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Foi fácil! Você achou mesmo que eu não encontraria os ovos?\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"Sim... Mas não tão rápido. Mas já que você foi tão bem neste desafio, o que acha de uma nova tarefa, mas desta vez muito mais difícil?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Claro. Mas você não está esquecendo de uma coisa coelhinho?\" $.$";
    next;
    mes "[Coelho Pascal]";
    mes "\"Hahaha, eu sabia que você ia chegar neste assunto. Você merece uma boa recompensa... Deixe-me pensar...\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"Já sei qual será a sua recompensa. E acho que você irá gostar muito pois é algo muito divertido. Tão divertido quanto foi a brincadeira de encontrar ovos de páscoa... sem contar que tem tudo a ver!\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"Mas primeiro feche os olhos para não estragar a surpresa!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Tudo bem!",                 L_fechaOsOlhos,
        "Adoro surpresas!",          L_fechaOsOlhos,
        "Nossa... quanto mistério!", L_fechaOsOlhos;

L_fechaOsOlhos:
    mes "Você fecha os olhos e então pode apenas escutar o coelho batendo em um objeto oco, mas não faz a menor ideia do que seja...";
    next;
    mes "E quanto mais ele demora em seu trabalho mais ansioso você fica...";
    next;
    mes "Mas ele finalmente termina o trabalho!";
    mes "";
    PASCOA2_esconde = 3;
    getinventorylist;
    if (@inventorylist_count >= 100) goto L_Cheio;
    goto L_darPremio1;

L_devePremio1:
    mes "[Coelho Pascal]";
    mes "\"Me lembro de você... estou de tevendo um prêmio!\"";
    next;
    getinventorylist;
    if (@inventorylist_count >= 100) goto L_Cheio;
    goto L_darPremio1;

L_darPremio1:
    mes "[Coelho Pascal]";
    mes "\"Veja só o que tenho para você! Espero que goste!\"";
    getitem @hatCascaOvo, 1;
    PASCOA2_esconde = 4;
    PASCOA2_escondeCont = 0;
    PASCOA2_escondeNum = 0;
    close;


L_desafio2:
    mes "[Coelho Pascal]";
    mes "\"Muito bem! Agora vamos ao novo desafio! Esse é bem mais difícil, pois você vai ter que vencer a MINHA astúcia!\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"A ideia é a mesma do desafio anterior. Só que quem vai esconder os ovos é você, e eu vou ter que encontrá-los!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"OK. Mas eu não queria gastar ovos de chocolate assim! Pode ser umas bolotas?\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"Bolotas? Nem pensar! Tem que ser ovos de chocolate. Mas não se preocupe, se seus ovos de chocolate acabarem eu posso fazer alguns para você.\"";
    next;
    mes "\"Só vou precisar de alguns ingredientes e de tinta guache para enfeitar.\"";
    PASCOA2_esconde = 5;
    PASCOA2_escondeCont = 0;
    PASCOA2_escondeNum = rand(6);
    next;
    goto L_MenuIngredienteOvo;

L_MenuIngredienteOvo:
    mes "[" + strcharinfo(0) + "]";
    menu
        "Você tem uma lista com os ingredientes?", L_ingredientes1,
        "Onde consigo os ingredientes e a tinta?", L_OndeIngredientes1,
        "Ok... vou procurar pelos materiais.",     L_vouProcurar;

L_vouProcurar:
    mes "[Coelho Pascal]";
    mes "\"Quando tiver materiais basta trazê-los para mim.\"";
    close;

L_ingredientes1:
    callsub L_ingredientes;
    goto L_MenuIngredienteOvo;

L_OndeIngredientes1:
    callsub S_ondeIngredientes;
    goto L_MenuIngredienteOvo;

L_MenuEsconderOvos:
    if (PASCOA2_escondeCont >= 30 ) goto L_escondeuTodos;

    @n = rand(2);
    mes "[Coelho Pascal]";
    if (@n == 0) mes "\"Então?! Já escondeu todos os ovos de páscoa?\"";
    if (@n == 1) mes "\"Olá novamente! Conseguiu muitos ingredientes?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Falta esconder alguns ovos.",             L_Nao2Escondi,
        "Quero fazer um [Ovo de Páscoa].",         L_MenuOvo,
        "Quero pintar meu [Chapéu Casca de Ovo].", L_MenuCascaOvo,
        "Vixi... tenho que ir!",                   L_Nao2Escondi;

L_Nao2Escondi:
    mes "[Coelho Pascal]";
    mes "\"Mal posso esperar para procurar por ovos de páscoa!\"";
    close;

L_escondeuTodos:
    mes "[Coelho Pascal]";
    mes "\"É o que estou pensando? Você já escondeu os 30 ovos para que eu possa procurá-los?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Tudo certo! Escondi todos os ovos.",                      L_foramEscondidos,
        "Sim! Só falta você ir procurá-los... ou tentar... hihi!", L_foramEscondidos,
        "Quero fazer outro [Ovo de Páscoa].",                      L_MenuOvo;

L_foramEscondidos:
    mes "[Coelho Pascal]";
    mes "\"Muito bem, você fez um ótimo trabalho.";
    mes "Acho que agora é minha vez de procurar por ovos de páscoa!\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"Me dê alguns minutos e eu encontrarei todos.\"";
    PASCOA2_esconde = 6;
    PASCOA2_escondeCont = 0;
    PASCOA2_escondeNum = 0;
    set PASCOA2_escondeTick, gettimetick(2); // segundos de 1970.
    close;

L_CoelhoProcuraOvos:
    set @tick, gettimetick(2); // segundos de 1970.
    if (@tick >= PASCOA2_escondeTick + 10 * 60) goto L_coelhoEncontrouOvos;
    set @n, (@tick - PASCOA2_escondeTick) / 20; //- 600/30 = 20
    mes "[Coelho Pascal]";
    if (@n == 0) mes "\"Não encontrei nenhum ovo de páscoa ainda :?\"";
    if (@n == 1) mes "\"Só encontrei um ovo de páscoa... mas vou encontrar mais ovos!\"";
    if (@n >= 2) mes "\"Já encontrei " + @n + " ovos de páscoa... e não vou parar até encontrar todos!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Quero fazer um [Ovo de Páscoa].",         L_MenuOvo,
        "Quero pintar meu [Chapéu Casca de Ovo].", L_MenuCascaOvo,
        "Tchau tchau.",                            L_Tchau;

L_Tchau:
    mes "[Coelho Pascal]";
    mes ":P";
    close;

L_coelhoEncontrouOvos:
    mes "[Coelho Pascal]";
    mes "\"Encontrei todos os ovos! Foi muito legal participar de minha própria brincadeira!\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"E como prometido você irá receber um prêmio por ter escondido os ovos de chocolate.\"";
    next;
    PASCOA2_esconde = 7;
    getinventorylist;
    if (@inventorylist_count >= 100) goto L_Cheio;
    goto L_darPremio2;

L_devePremio2:
    mes "[Coelho Pascal]";
    mes "\"Já posso dar seu prêmio? É um prêmio muito bom!\"";
    next;
    getinventorylist;
    if (@inventorylist_count >= 100) goto L_Cheio;
    goto L_darPremio2;

L_darPremio2:
    mes "[Coelho Pascal]";
    mes "\"Veja só o que tenho para você! Este prêmio é ainda melhor que o primeiro!\"";
    getitem @orelhasLebre, 1;
    PASCOA2_esconde = 8;
    PASCOA2_escondeCont = 0;
    PASCOA2_escondeNum = 0;
    PASCOA2_escondeTick = 0;
    close;


L_desafio3:
    setarray @interessado$, "interessada", "interessado";
    mes "[Coelho Pascal]";
    mes "\"Ainda tenho um ultimo desafio a você... mas este é muito mais difícil que todos os outros.";
    mes "Estaria " + @interessado$[Sex] + "?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Mais é claro!",         L_AceitaDesafio3,
        "Quem sabe outro hora.", L_Tchau2;

L_Tchau2:
    mes "[Coelho Pascal]";
    mes "\"Tudo bem.. quando estiver pronto para o desafio venha falar comigo.\"";
    close;

L_AceitaDesafio3:
    mes "[Coelho Pascal]";
    mes "\"Agora você deve encontrar ovos de páscoa especiais. O único que tem um embrulho prateado.";
    mes "Legal né!\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"Eu só escondi 10 destes ovos e posso garantir que eles estão bem escondidos. Não vai ser nada fácil encontrá-los.\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"E tem mais, eu não os escondi apenas aqui... eles podem estar em qualquer lugar do Mundo de Mana.";
    mes "Eu realmente me superei neste desafio!!! Mas sei que você gosta deste tipo de desafio!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Realmente adoro desafios!",    L_Continua,
        "Faço tudo por um bom prêmio.", L_Continua,
        "Pô... agora você exagerou!",   L_Continua;

L_Continua:
    mes "[Coelho Pascal]";
    mes "\"Uma dica... os esconderijos fora deste mapa estão em grupos de 5. Onde você encontrar um deverá encontrar mais 4 no memo mapa.\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"Heheh... Boa sorte!\"";
    PASCOA2_esconde = 9;
    PASCOA2_escondeCont = 0;
    PASCOA2_escondeNum = rand(54);
    close;

L_procuraOvosPrata:
    if (PASCOA2_escondeCont == 0) goto L_nenhumOvoPrata;
    if (PASCOA2_escondeCont >= 10) goto L_todosOvosPrata;

    mes "[Coelho Pascal]";
    mes "\"Sorte na busca dos 10 ovos de páscoa prateados?\"";
    mes "[" + strcharinfo(0) + "]";
    menu
        "Só um pouco de sorte.",                   L_boaSorte,
        "Quero fazer um [Ovo de Páscoa].",         L_MenuOvo,
        "Quero pintar meu [Chapéu Casca de Ovo].", L_MenuCascaOvo,
        "Vou procurar mais ovos.",                 L_boaSorte;

L_boaSorte:
    mes "[Coelho Pascal]";
    mes "\"Ok... só não se esqueça que não os escondi apenas aqui... eles podem estar em qualquer lugar do Mundo de Mana.";
    mes "E também que os esconderijos fora deste mapa estão em grupos de 5. Onde você encontrar um deverá encontrar mais 4 no memo mapa.\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"Boa sorte!\"";
    close;

L_nenhumOvoPrata:
    mes "[Coelho Pascal]";
    mes "\"É... eu me superei mesmo.";
    mes "Você ainda não encontrou nenhum!\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"Eu escondi os 10 ovos de páscoa prateado muito bem escondidos! Não vai ser nada fácil encontrá-los.\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"E tem mais, eu não os escondi apenas aqui... eles podem estar em qualquer lugar do Mundo de Mana.";
    mes "Eu realmente me superei neste desafio!!!\"";
    next;
    mes "\"Uma dica... os esconderijos fora deste mapa estão em grupos de 5. Onde você encontrar um deverá encontrar mais 4 no memo mapa.\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"Heheh... Boa sorte!\"";
    close;

L_todosOvosPrata:
    mes "[Coelho Pascal]";
    mes "\"Você conseguiu mais uma vez!";
    mes "Você foi muito bom em todos os meus desafios! E isso foi incrível!\"";
    next;
    mes "[Coelho Pascal]";
    mes "Agora só me resta duas coisas a te contar:";
    mes "A primeira é que você venceu todos os meus desafios.";
    mes "A segunda é que você acaba de ganhar mais um prêmio!\"";
    next;
    PASCOA2_esconde = 10;
    getinventorylist;
    if (@inventorylist_count >= 100) goto L_Cheio;
    goto L_darPremio3;

L_devePremio3:
    mes "[Coelho Pascal]";
    mes "\"Já posso dar seu prêmio? É um prêmio muito bom!\"";
    next;
    getinventorylist;
    if (@inventorylist_count >= 100) goto L_Cheio;
    goto L_darPremio3;

L_darPremio3:
    mes "[Coelho Pascal]";
    mes "\"Veja só o que tenho para você! Tudo a ver com a páscoa!\"";
    getitem @bonePascoa, 1;
    PASCOA2_esconde = 11;
    PASCOA2_escondeCont = 0;
    PASCOA2_escondeNum = 0;
    close;


L_MenuOvo:
    @idOvoPascoa = 1208;
    @idChocolate = 3071;
    @idCastanha = 3075;
    @idTintaGuache = 3080;

    mes "";
    mes "[Coelho Pascal]";
    mes "\"Qual tipo de ovo de páscoa você quer fazer.";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Sabor Cuputale Especial.",         L_fazerOvos,
        "Sabor Chocolate Especial.",        L_fazerOvos,
        "Sabor Chocolate Amargo Especial.", L_fazerOvos,
        "Sabor Chocolate Branco Especial.", L_fazerOvos,
        "Quero rever os ingredientes.",     L_ingredientes2,
        "Onde consigo os ingredientes?",    L_OndeIngredientes2,
        "Desistir.",                        L_Fechar;

L_ingredientes2:
    callsub L_ingredientes;
    goto L_MenuOvo;

L_OndeIngredientes2:
    callsub S_ondeIngredientes;
    goto L_MenuOvo;

L_fazerOvos:
    if (@menu > 4) close;
    @ovotipo = @menu - 1;
    mes "[Coelho Pascal]";
    mes "\"Me dê seus ingredientes.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Aqui estão.",                        L_aquiEstao,
        "Ops... acabaram meus ingredientes.", L_Fechar;

L_aquiEstao:
    if (countitem(@idChocolate + @ovotipo) < 10 || countitem(@idCastanha + @ovotipo) < 5) goto L_semIngredientes;
    goto L_misturando;

L_semIngredientes:
    mes "[Coelho Pascal]";
    mes "\"Você não tem ingredientes suficiente para fazer o ovo de páscoa que escolheu.";
    goto L_MenuOvo;

L_semTinta:
    mes "[Coelho Pascal]";
    mes "\"Eu acho que você não tem essa cor de Tinta Guache.\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"Escolha uma outra cor.\"";
    goto L_MenuCor;

L_misturando:
    mes "[Coelho Pascal]";
    mes "\"Ótimo, vamos começar a trabalhar...\"";
    mes "";
    mes "O coelho pega os ingredientes e mistura tudo com um pouco de leite em uma vasilha ao fogo. Espera um pouco até que o chocolate derreta e então o coloca em duas fôrmas ovais.";
    next;
    goto L_fazerOvo3;

L_fazerOvo3:
    mes "[Coelho Pascal]";
    mes "\"Enquanto o chocolate endurece...";
    mes "Que cor gostaria de pintar seu ovo de páscoa? Que cor de [Tinta Guache] você tem ai?!\"";
    next;
    goto L_MenuCor;

L_MenuCor:
    mes "[" + strcharinfo(0) + "]";
    menu
        "Vermelho",                        L_CorEscolhida,
        "Verde",                           L_CorEscolhida,
        "Azul",                            L_CorEscolhida,
        "Amarelo",                         L_CorEscolhida,
        "Laranjado",                       L_CorEscolhida,
        "Rosa",                            L_CorEscolhida,
        "Não tenho nenhuma Tinta Guache.", L_Fechar;

L_CorEscolhida:
    @ovocor = @menu - 1;
    if (countitem(@idTintaGuache + @ovocor) < 1) goto L_semTinta;
    goto L_fazerOvo4;

L_fazerOvo4:
    mes "[Coelho Pascal]";
    mes "\"Já temos o chocolate, a tinta... deixe-me ver o que mais está faltando...\"";
    next;
    mes "";
    mes "\"Acho que temos tudo...\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"Vamos decorar o ovo de páscoa.\"";
    mes "";
    mes "O coelho encaixa os dois lados do ovo e o enrola em uma folha de alumínio. Recorta algumas tiras de papel e também as enrola em volta do ovo passando um pouco de cola nas pontas. Em seguida molha a ponta do pincel com tinta e começa a pintar o ovo.";
    next;
    mes "[Coelho Pascal]";
    mes "\"Quase terminando...\"";
    mes "";
    mes "Depois de mais alguns acabamentos ele finalmente o termina.";
    next;
    goto L_fazerOvo6;

L_fazerOvo6:
    getinventorylist;
    if (@inventorylist_count >= 100 ) goto L_Cheio;
    if (countitem(@idChocolate + @ovotipo) < 10 || countitem(@idCastanha + @ovotipo) < 5) goto L_semIngredientes;
    if (countitem(@idTintaGuache + @ovocor) < 1) goto L_semTinta;
    mes "[Coelho Pascal]";
    mes "\"Aqui está seu ovo de páscoa. Agora você já pode participar da brincadeira!\"";
    delitem @idChocolate + @ovotipo, 10;
    delitem  @idCastanha + @ovotipo, 5;
    delitem @idTintaGuache + @ovocor, 1;
    getitem @idOvoPascoa + @ovocor, 1;
    close;


L_MenuCascaOvo:
    @hatCascaOvo = 1256;
    @hatCascaOvoCor = 3230;
    @idTintaGuache = 3080;

    if (countitem(@hatCascaOvo) < 1) goto L_semCascaOvo;
    mes "";
    mes "[Coelho Pascal]";
    mes "\"Posso pintar o seu Chapéu Casca de Ovo de várias cores e vários formatos.";
    mes "Com um pouco de imaginação e tinta guache tudo é possível.\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"Gostaria de pintar o seu chapéu?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Sim, quero pintar.",  L_qualCorCascaOvo,
        "Agora não, obrigad" + @fm$ + ".", L_agoraNao;

L_agoraNao:
    mes "[Coelho Pascal]";
    mes "\"Ok... quando quiser, já sabe quem procurar.\"";
    close;

L_qualCorCascaOvo:
    mes "[Coelho Pascal]";
    mes "\"Ótimo... então vamos pintar o seu chapéu.";
    mes "Gostaria de pintar de qual cor?\"";
    goto L_MenuCor2;

L_MenuCor2:
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Vermelho",                        L_CorEscolhida2,
        "Verde",                           L_CorEscolhida2,
        "Azul",                            L_CorEscolhida2,
        "Amarelo",                         L_CorEscolhida2,
        "Laranjado",                       L_CorEscolhida2,
        "Rosa",                            L_CorEscolhida2,
        "Não tenho nenhuma Tinta Guache.", L_Fechar;

L_CorEscolhida2:
    @cor = @menu - 1;
    if (countitem(@idTintaGuache + @cor) < 1) goto L_semTinta2;
    goto L_pintarCascaOvo1;

L_semTinta2:
    mes "[Coelho Pascal]";
    mes "\"Eu acho que você não tem essa cor de Tinta Guache.\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"Escolha uma outra cor.\"";
    goto L_MenuCor2;

L_semCascaOvo:
    mes "[Coelho Pascal]";
    mes "\"Espere... eu preciso que você tenha um [Chapéu Casca de Ovo] que ainda não foi pintado para que eu poça pintá-lo.\"";
    close;

L_pintarCascaOvo1:
    mes "[Coelho Pascal]";
    mes "\"Muito bem... já sabemos qual cor pintar. Agora só falta pensar no que pintar...\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"Hum... acho que já sei!";
    mes "Sim... vai ficar muito bom. Já podemos começar.\"";
    next;
    mes "[Coelho Pascal]";
    mes "\"Primeiro vamos preparar a tinta com um pouco deste fixador, para que não seja facilmente removida.\"";
    mes "";
    mes "O coelho pinga uma única gota de um líquido viscoso em seu pote de tinta e o mistura com o pincel.";
    next;
    mes "[Coelho Pascal]";
    mes "\"Agora preciso pintar o seu chapéu rapidamente antes que o fixador seque a tinta e ela não sirva mais para pintar.\"";
    mes "";
    mes "Com muita tranquilidade e concentração o coelho opera o pincel em seu chapéu criando uma bela pintura.";
    next;
    getinventorylist;
    if (@inventorylist_count >= 100 ) goto L_Cheio;
    if (countitem(@hatCascaOvo) < 1) goto L_semCascaOvo;
    if (countitem(@idTintaGuache + @cor) < 1) goto L_semTinta2;
    mes "[Coelho Pascal]";
    mes "\"Aqui está seu novo Chapéu Casca de Ovo. Assim todo colorido ele é muito mais bonito!\"";
    delitem @hatCascaOvo, 1;
    delitem @idTintaGuache + @cor, 1;
    getitem @hatCascaOvoCor + (rand(5) * 6) + @cor, 1; // Um modelo aleatório do chapéu
    close;


L_ingredientes:
    mes "[Coelho Pascal]";
    mes "\"Para fazer um delicioso ovo de páscoa de 340g precisaremos de 250g de chocolate especial e 90g de castanhas.";
    mes "Mas não pode ser qualquer tipo de chocolate especial e nem qualquer tipo de castanha. Para cada sabor de chocolate especial temos que utilizar um tipo diferente de castanha.\"";
    mes "";
    mes "\"Veja a lista.\"";
    next;
    mes "[Lista de Ingredientes]";
    mes "Ovo de páscoa sabor Cuputale Especial:";
    mes "* 10 [" + getitemlink("CupulateEsp") + "]";
    mes "*  5 [" + getitemlink("CastanhaPara") + "]";
    mes "";
    mes "Ovo de páscoa sabor Chocolate Especial:";
    mes "* 10 [" + getitemlink("ChocolateEsp") + "]";
    mes "*  5 [" + getitemlink("Amendoim") + "]";
    mes "";
    mes "Ovo de páscoa sabor Chocolate Amargo Especial:";
    mes "* 10 [" + getitemlink("ChocolateAmargoEsp") + "]";
    mes "*  5 [" + getitemlink("CastanhaCaju") + "]";
    mes "";
    mes "Ovo de páscoa sabor Chocolate Branco Especial:";
    mes "* 10 [" + getitemlink("ChocolateBrancoEsp") + "]";
    mes "*  5 [" + getitemlink("Coco") + "]";
    next;
    return;

S_ondeIngredientes:
    mes "[Coelho Pascal]";
    mes "\"Coelhos adoram chocolate e castanhas... aposto que você vai conseguir todos os ingredientes com eles. Mas tenha cuidado para não machucá-los. Já a tinta guache você deve encontrar fácil com algum vendedor por aí...\"";
    next;
    return;

L_Cheio:
    mes "[Coelho Pascal]";
    mes "\"Espere um momento... você tem muita bagagem!";
    mes "Seria melhor você me procurar mais tarde.";
    close;

L_Fechar:
    close;
}
