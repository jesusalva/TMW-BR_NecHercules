

froz001,45,85,0	script	Karu	NPC571,10,10,{
    @AntenaLuminosa = 612;
    @AntenaRosa = 614;
    @GrimoriumNinja = 3211;
    @ClasseCavaleiro = 400;
    @ClasseDruida = 401;
    @ClasseMago = 402;
    @ClasseNinja = 403;
    @ClassePaladino = 404;
    @ClasseNecromante = 405;

    if (getskilllv(@ClasseNinja) == 0 && (QUEST_Classe < 2 || BaseLevel < 40)) goto L_SemNivel;
    if (getskilllv(@ClasseNinja) == 0 && QUEST_Classe == 3) goto L_Retorno;
    if (getskilllv(@ClasseNinja) == 0 && QUEST_Classe == 4) goto L_JaDoutraClasse;
    if (getskilllv(@ClasseNinja) >= 1) goto L_JaDestaClasse;
    goto L_IniciaQuest;


L_JaDoutraClasse:
    mes "[Karu]";
    mes "\"Agora você já pertence a classe...\"";

    @Classe = @ClasseCavaleiro;
    callfunc "GerarNomeDaPatente"; 
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;
    
    @Classe = @ClasseDruida;
    callfunc "GerarNomeDaPatente"; 
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;

    @Classe = @ClasseMago;
    callfunc "GerarNomeDaPatente"; 
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;

    @Classe = @ClasseNinja;
    callfunc "GerarNomeDaPatente"; 
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;

    @Classe = @ClassePaladino;
    callfunc "GerarNomeDaPatente"; 
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;
    
    @Classe = @ClasseNecromante;
    callfunc "GerarNomeDaPatente"; 
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;

    close;

L_JaDestaClasse:
    @Classe = @ClasseNinja;
    callfunc "GerarNomeDaPatente"; 
    mes "[Karu]";
    if (Sex==0)mes "\"Meus parabéns! Agora você é uma "+@NomeDaPatente$+"!\"";
    if (Sex==1)mes "\"Meus parabéns! Agora você é um "+@NomeDaPatente$+"!\"";
    close;

L_SemNivel:
    mes "[Karu]";
    mes "\"Konnichiwa, viajante!\"";
    close;

L_IniciaQuest:
    mes "Você vê um homem muito furtivo, e que parece ser bem ágil.";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "(^_^) \"O senhor é o Mestre dos Ninjas?\"";
    next;
    mes "[Karu]";
    mes "(^_^) \"Haya! Sim! Meu nome é Karu, o Mestre dos Ninjas do Mundo de Mana.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    if (Sex == 0) mes "(^_^) \"O Mestre Karu poderia me ensinar a ser uma Ninja?\"";
    if (Sex == 1) mes "(^_^) \"O Mestre Karu poderia me ensinar a ser um Ninja?\"";
    next;
    mes "[Karu]";
    mes "(^_^) \"Hum! Talvez...";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "(·_·) Por favor, belo mestre! O senhor gostará de minha ajuda!", L_Falha,
        "(T.T) Se não me aceitar, digo que o senhor colocou meleca do nariz no copo da princesa!", L_Falha,
        "(X.x) Preciso ser aceito ou levarei uma surra de minha mamãe!", L_Falha,
        "(-_-) Posso prová-lo que serei um excelente aprendiz!", L_Provar,
        "(^_^) Faço qualquer coisa se me aceitar! Exceto aos domingos, claro!", L_Falha,
        "(^_^) Tenho certeza um homem tão bacana como o senhor irá me aceitar! ", L_Falha,
        "(¬.¬) Não preciso de sua ajuda. Eu nem queria mesmo!", L_Falha;

L_Falha:
    mes "";
    mes "[Karu]";
    mes "(-_-) \"Saia daqui antes que eu te arranque a cabeça com um só golpe!\"";
    close;

L_Provar:
    mes "";
    mes "[Karu]";
    mes "(^_^) \"Isso sim me parece interessante!\"";
    next;
    mes "[Karu]";
    mes "(T_T) \"Mas Para provar seu valor, preciso que você me traga alguns itens.\"";
    next;
    mes "[Karu]";
    mes "(¬.¬) \"Eles são: 50 antenas luminosas e 30 antenas rosas.\"";
    next;
    mes "[Karu]";
    mes "(T_T) \"Mas antes saiba que se você escolher ser da classe dos Ninjas não poderá aprender outra classe depois. Também é preciso saber se você é realmente excepcional, e se sabe o que está fazendo.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "(^-^) Como o senhor quiser! Eu já trago o que me foi ordenado.", L_Trago,
        "(T.T) Ahh, esquece! Desisto desta porcaria!", L_Fechar;

L_Trago:
    QUEST_Classe = 3;
    mes "";
    mes "[Karu]";
    mes "(¬.¬) \"Boa sorte!\"";
    close;

L_Retorno:
    mes "[Karu]";
    mes "\"(^-^) Trouxe os itens que ordenei?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "(^_^) Sim, aqui estão.", L_Pronto,
        "(X.x) Não, Esqueci quais são!", L_Esqueci;

L_Pronto:
    if (countitem(@AntenaLuminosa)<50 || countitem(@AntenaRosa)<30) goto L_Esqueci;
    getinventorylist;
    if (@inventorylist_count >=100) goto L_SemLugar;
    delitem @AntenaLuminosa, 50;
    delitem @AntenaRosa, 30;
    getitem @GrimoriumNinja, 1;
    addtoskill @ClasseNinja, 1;
    QUEST_Classe = 4;
    mes "";
    mes "[Karu]";
    mes "(^_^) \"Omedetou gozaimasu! Você provou ser capaz de se esforçar. Agora você é um aprendiz dos Ninjas.\"";
    next;
    mes "[Karu]";
    mes "(^_^) \"Eu te dou de presente esse Grimorium Rubro que é o livro para você anotar os segredos guia dos Ninjas.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Omedetou gozaimasu? O.o\"";
    next;
    mes "[Karu]";
    mes "(^_^) \"Parabéns!\"";
    close;

L_Esqueci:
    mes "";
    mes "[Karu]";
    mes "(¬_¬) \"Bem, você não trouxe o que eu te pedi.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "(X.x) \"Opa! Acho que perdi alguns itens por aeee.\"";
    next;
    mes "[Karu]";
    mes "(T_T) \"Não esqueça que os itens são esses!";
    mes " * 50 Antenas Luminosas.";
    mes " * 30 Antenas Rosa.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "(X.x) \"Senhor! Sim senhor! Não esquecerei senhor! Já volto senhor!\"";
    close;

L_SemLugar:
    mes "[Karu]";
    mes "\"Sua bolsa está bem cheia, não é? Libere espaço e depois volte para pegar seu presente.\"";
    close;

L_Fechar:
    close;

OnTouch:
    @ClasseNinja = 403;
    if (getskilllv(@ClasseNinja) == 0 && (QUEST_Classe < 2 || BaseLevel < 40)) end;
    emotion EMOTE_QUEST;
    end;

}
