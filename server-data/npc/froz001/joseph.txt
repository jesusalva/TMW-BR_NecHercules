

froz001,144,23,0	script	Joseph	NPC523,7,7,{
    @TecidoDeAlgodao = 660;
    @TintaAzulEscura = 692;
    @CasuloDeCeda = 718;
    @ErvaCobalto = 681;
    @BoneAFK = 3018;
    
    if ($PosseDeFroz != 0 && Reino != $PosseDeFroz) goto L_SugestaoAoInvasor;
    if (QUEST_BoneAFK == 0 && BaseLevel < 40) goto L_SemLevel;
    if (QUEST_BoneAFK == 1) goto L_Retorno;
    if (QUEST_BoneAFK == 2) goto L_Fim;
    goto L_IniciaQuest;

L_SugestaoAoInvasor:
    heal -(MaxHp / 10), 0;
    @n = rand(4);
    if (@n == 0) npctalk strnpcinfo(0), "Saia daqui seu invasor de Froz! (Chute!)";
    if (@n == 1) npctalk strnpcinfo(0), "Você não pode entrar em Froz! (Pontapé!)";
    if (@n == 2) npctalk strnpcinfo(0), "Como você se atreve a tentar machucar as pessoas do Reino de Froz? (Soco!)";
    if (@n == 3) npctalk strnpcinfo(0), "Saia daqui ou eu te mato! (Soco!)";
    end;

L_SemLevel:
    mes "[Joseph]";
    mes "\"Estou ocupado neste momento!\"";
    next;
    mes "[Joseph]";
    mes "\"Não tenho tempo para você!\"";
    close;

L_IniciaQuest:
    mes "[Joseph]";
    mes "\"Ufa! Voltei! Desculpe por não ter lhe respondido. Eu estava fazendo minhas necessidades.\"";
    next;
    mes "[Joseph]";
    mes "\"O que você quer mesmo?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Estou procurando o caminho para Brhamir.",                     L_Caminho,
        "Quando te vi com esse boné engraçado, desejei falar contigo.", L_Engracado,
        "Estava procurando um lugar para treinar.",                     L_Upar;

L_Upar:
    mes "";
    mes "[Joseph]";
    mes "\"Não faço a minima ideia de onde você possa treinar.\"";
    close;

L_Caminho:
    mes "";
    mes "[Joseph]";
    mes "=X \"Não sei te informar.\"";
    close;

L_Engracado:
    mes "";
    mes "[Joseph]";
    mes "Ha! Ha! Ha! Ha! Ha! Ha! Ha! Ha! ";
    mes "\"Também acho este bonê muito engraçado. Ele também é muito útil para sinalizar que não estou diponível para conversar.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Que legal! Isso é uma ótima ideia. Parabéns!", L_Parabens,
        "Nossa! É isso que estou precisando.",          L_Precisando,
        "=X Acho que não é tão útil como você diz.",    L_Tchau;

L_Tchau:
    mes "";
    mes "[Joseph]";
    mes "=( \"Será?!?!\"";
    close;

L_Parabens:
    mes "";
    mes "[Joseph]";
    mes "=D \"Obrigado!\"";
    close;

L_Precisando:
    mes "";
    mes "[Joseph]";
    mes "=) \"Posso fazer um para você contanto que me traga alguns itens!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Quais itens você precisa?",              L_Necessita,
        "Estou sem tempo agora. Noutrora volto!", L_Outrora;

L_Outrora:
    mes "";
    mes "[Joseph]";
    mes "\"Ok! Ficarei esperando.\"";
    close;

L_Necessita:
    mes "";
    mes "[Joseph]";
    mes "\"Preciso de:";
    mes " *  25 Tecidos de Algodão, Para a confecção do boné.";
    mes " * 300 Casulos de seda, Para os detalhes.";
    mes " *  01 Tinta na cor Azul escura, Para pintar seu boné";
    mes " *  30 Ervas Cobalto, Para fazer algumas partes mais escuras.";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu 
        "Ok! Já volto com este ítens.", L_Aceitar,
        "Deixa para outro dia!",        L_Tchau2;

L_Tchau2:
    mes "";
    mes "[Joseph]";
    mes "=S \"Ok! Você é quem sabe.\"";
    close;

L_Aceitar:
    mes "";
    mes "[Joseph]";
    mes "\"Ok! Ficarei esperando.\"";
    QUEST_BoneAFK = 1;
    close;
   
L_Retorno:
    mes "[Joseph]";
    mes "\"Olá " + @my$ + " amig" + @fm$ + "! Trouxe os itens de seu boné?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu 
        "Aqui estão!",                 L_Pronto,
        "Esqueci o que você precisa!", L_ItensNecessarios;

L_Pronto:
    if (countitem(@TecidoDeAlgodao) < 25 || countitem(@TintaAzulEscura) < 1 || countitem(@CasuloDeCeda) < 300 || countitem(@ErvaCobalto) < 30) goto L_Nao2tem;
    getinventorylist;
    if (@inventorylist_count == 100) goto L_SemEspaco;
    delitem @TecidoDeAlgodao, 25;
    delitem @TintaAzulEscura, 1;
    delitem @CasuloDeCeda, 300;
    delitem @ErvaCobalto, 30;    
    getitem @BoneAFK, 1;
    QUEST_BoneAFK = 2;
    mes "";
    mes "[Joseph]";
    mes "=) \"Como eu já possuía alguns dos itens aqui comigo, fiz o boné adiantado.\"";
    next;
    mes "[Joseph]";
    mes "=D \"Pegue seu boné.\"";
    close;

L_Nao2tem:
    mes "";
    mes "[Joseph]";
    mes "\"Acho que você não tem tudo. Traga as coisas que eu te disse para que eu possa fazer o seu boné.\"";
    close;

L_ItensNecessarios:
    mes "";
    mes "[Joseph]";
    mes "\"Deseja que eu diga novamente os itens necessários?\"";
    next;    
    mes "[" + strcharinfo(0) + "]";
    mes "\"Sim, claro!\"";
    next;    
    mes "[Joseph]";
    mes "\"Os itens são estes:";
    mes " *  25 Tecidos de Algodão, para confeccionar o boné.";
    mes " *   1 Tinta na Azul Escura, para pintar seu boné";
    mes " * 300 Casulos de Seda, para os detalhes.";
    mes " *  30 Ervas Cobalto, para fazer as partes mais escuras.\"";    
    next;
    mes "[Joseph]";
    mes "=D \"Dessa vez não esqueça!\"";
    close;

L_SemEspaco:
    mes "";
    mes "[Joseph]";
    mes "\"Ei amiguinh" + @fm$ + "! Volte depois!\"";
    next;
    mes "[Joseph]";
    mes "=S \"Acho que seu boné vai amassar se eu colocá-lo em sua mochila cheia!\"";
    close;

L_Fim:
    mes "[Joseph]";
    mes "=D \"Lembre-se: sempre equipe o boné se for ficar longe do teclado.\"";
    close;

OnTouch:
    if ($PosseDeFroz != 0 && Reino != $PosseDeFroz) goto L_Raiva;
    if (QUEST_BoneAFK >= 2 && BaseLevel < 40) end;
    emotion EMOTE_QUEST;
    end;

L_Raiva:
    emotion EMOTE_UPSET;
    heal -(MaxHp / 10), 0;
    end;
}
