009,32,41,0	script	#Menir-009	NPC144,{
    @map$ = "009";
    @x = 32;
    @y = 42;
    callfunc "SoulMenhir";
    close;
}

009,32,42,0	script	#Penalizador-009	NPC32767,1,1,{
    callfunc "limparDist";
    end;
}

009,31,40,0	script	#ZonaPerigo-009	NPC32767,3,3,{
    if (ZONA_SEGURA == 1) message strcharinfo(0), "Estou fora da zona segura.";
    set ZONA_SEGURA, 0; // Na saída do PVP a zona deixa de ser segura
    close;
}
