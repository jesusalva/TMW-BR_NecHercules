function	script	drop_ambar_pedraAguia	{
    if (QUEST_pedraAguia != 1 || DROP_pedraAguia > 15) goto L_Return;

    @n = rand(100);
    if (@n < 5) getitem 3215, 1; // Ambar
    if (@n < 5) set DROP_pedraAguia, DROP_pedraAguia + 1;
    return;

L_Return:
    return;
}


009,80,63,0	script	Lobo Solitário	NPC143,{
    if (!(isin("009", 76, 61, 88, 67))) goto L_muitoLonge;

    @LEATHER_PATCH = 708;
    @SNAKE_SKIN = 641;
    @Raiz = 740;
    @pedraAmbar = 3215;
    @tocoMadeira = 569;
    @GarrafaVazia = 540;
    @pedraOlhoAguia = 3216;

    @LEATHER_PATCH_PRICE = 300;

    @wants_leather_patch = QUEST_Forestbow_state & NIBBLE_4_MASK;

    //- Quest Pedra Olho de Aguia - Fazer Quest Arco da Floresta primeiro.
    if (QUEST_MASK1 & MASK1_PEDRA_AGUIA) goto L_Inicio; // Quest Pedra Olho de Aguia concluída. Estado 3.
    if (QUEST_colarAguia != 1) goto L_Inicio;

    if (QUEST_pedraAguia == 0) goto L_Questpedra;
    if (QUEST_pedraAguia == 1) goto L_retornapedra;
    if (QUEST_pedraAguia == 2) goto L_criarPedraAguia;
    goto L_Inicio;

L_Inicio:
    mes "[Lobo Solitário]";
    mes "\"How!\"";
    next;

    if (getequipid(equip_head) == 643 || getequipid(equip_head) == 644) goto L_WearingCowboy;
    if (getequipid(equip_legs) == 642) goto L_WearingChaps;

    mes "[Lobo Solitário]";
    mes "\"Por gerações, minha tribo têm criado roupas especiais de diferentes itens.\"";
    goto L_Check_Shops;

L_muitoLonge:
    mes "[Lobo Solitário]";
    mes "\"Não posso te ouvir direito dessa distância.\"";
    close;

L_Check_Shops:
    if ((countitem(610) > 0 && countitem(@SNAKE_SKIN) > 9) && (countitem(524) > 0 && countitem(@SNAKE_SKIN) > 1)) goto L_Super_store;
    if (countitem(610) > 0 && countitem(@SNAKE_SKIN) > 9) goto L_Chaps_store;
    if (countitem(524) > 0 && countitem(@SNAKE_SKIN) > 1) goto L_Cowboy_store;
    next;
    mes "[Lobo Solitário]";
    mes "\"Talvez se você me trouxer o material certo, eu possa fazer algo para você.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    if (!@wants_leather_patch)
        menu
            "Você é um índio?! Me conte mais sobre você.", L_historia_indio,
            "OK, tchau.",                                  L_Fechar;
    menu
        "Você é um índio?! Me conte mais sobre você.",           L_historia_indio,
        "Espere, você pode fazer um retalho de couro para mim?", L_leather_patch,
        "OK, tchau.",                                            L_Fechar;

L_historia_indio:
    mes "";
    mes "[Lobo Solitário]";
    mes "\"Sim, eu sou índio. Meu pai é o grande Chefe Monte Brilhante, e minha mãe, a bela Lua do Grande Lago.";
    mes "Há muito tempo vivo distante de minha tribo e criar itens utilizando técnicas aperfeiçoadas por várias gerações de minha tribo é uma forma de me sentir mais próximo dela.\"";
    close;

S_CheckStuff:
    mes "[Lobo Solitário]";
    mes "\"Deixe-me ver o que você tem aí.\"";
    next;
    return;

L_Super_store:
    callsub S_CheckStuff;
    mes "[Lobo Solitário]";
    mes "\"Ahh você tem muitos artigos bons para se trabalhar.\"";
    mes "";
    mes "\"Com eles posso fazer se você quiser um chapéu de vaqueiro ou uma calça de couro de cobra.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    if (@wants_leather_patch)
        menu
            "Chapéu de vaqueiro, por favor.",       L_BuyCowboy,
            "Calça de couro de cobra soa bem.",     L_BuyChaps,
            "Você pode fazer um retalho de couro?", L_leather_patch,
            "Não agora, talvez depois.",            L_Fechar;
    menu
        "Chapéu de vaqueiro, por favor.",   L_BuyCowboy,
        "Calça de couro de cobra soa bem.", L_BuyChaps,
        "Não agora, talvez depois.",        L_NoDeal;

L_Cowboy_store:
    callsub S_CheckStuff;
    mes "[Lobo Solitário]";
    mes "\"Para lhe fazer um chapéu de vaqueiro vou precisar de:";
    mes "1 [" + getitemlink("ChapeuFantasia") + "]";
    mes "2 [" + getitemlink("PeleDeCobraDaMontanha") + "]";
    mes "5.000 GP\"";
    mes "";
    mes "\"Nós temos um acordo?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    if (@wants_leather_patch)
        menu
            "Sim, tudo bem.",                       L_BuyCowboy,
            "Você pode fazer um retalho de couro?", L_leather_patch,
            "Pensando bem, talvez mais tarde.",     L_NoDeal;
    menu
        "Sim, tudo bem.",                   L_BuyCowboy,
        "Pensando bem, talvez mais tarde.", L_NoDeal;

L_Chaps_store:
    callsub S_CheckStuff;
    mes "[Lobo Solitário]";
    mes "\"Para lhe fazer uma calça de couro de cobra vou precisar de:";
    mes " 1 [" + getitemlink("ShortJeans") + "]";
    mes "10 [" + getitemlink("PeleDeCobraDaMontanha") + "]";
    mes "10.000 GP\"";
    mes "";
    mes "[Lobo Solitário]";
    mes "\"Nós temos um acordo?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    if (!@wants_leather_patch)
        menu
            "Sim, tudo bem.",                   L_BuyChaps,
            "Pensando bem, talvez mais tarde.", L_NoDeal;
    menu
        "Sim, tudo bem.",                       L_BuyChaps,
        "Você pode fazer um retalho de couro?", L_leather_patch,
        "Pensando bem, talvez mais tarde.",     L_NoDeal;

L_BuyChaps:
    if (Zeny < 10000) goto L_NoMoney;
    if (countitem(610) < 1) goto L_NoJeans;
    if (countitem(@SNAKE_SKIN) < 10) goto L_NoSkins;
    Zeny = Zeny - 10000;
    delitem @SNAKE_SKIN, 10;
    delitem 610, 1;
    getitem 642, 1;
    goto L_DealDone;

L_BuyCowboy:
    if (Zeny < 5000) goto L_NoMoney;
    if (countitem(524) < 1) goto L_NoFancy;
    if (countitem(@SNAKE_SKIN) < 2) goto L_NoSkins;
    Zeny = Zeny - 5000;
    delitem @SNAKE_SKIN, 2;
    delitem 524, 1;
    @temp = rand(2);
    if (@temp == 0) goto L_Cowboy_white;
    goto L_Cowboy_black;

L_Cowboy_white:
    getitem 643, 1;
    goto L_DealDone;

L_Cowboy_black:
    getitem 644, 1;
    goto L_DealDone;

L_leather_patch:
    mes "[Lobo Solitário]";
    mes "\"Se você quiser apenas uma retalho de couro, então sim, posso fazer isso. Traga-me um [Pele de Cobra da Montanha] e " + @LEATHER_PATCH_PRICE + " GP.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Aqui está!",               L_EuTenho,
        "OK, Eu volto mais tarde.", L_End,
        "Isso é muito caro!.",      L_NoDeal;

L_EuTenho:
    if (countitem(@SNAKE_SKIN) < 1) goto L_NoSkins;
    if (Zeny < @LEATHER_PATCH_PRICE) goto L_NoMoney;

    Zeny = Zeny - @LEATHER_PATCH_PRICE;
    delitem @SNAKE_SKIN, 1;
    getitem @LEATHER_PATCH, 1;
    goto L_DealDone;

L_DealDone:
    mes "[Lobo Solitário]";
    mes "\"Aqui está!\"";
    mes "";
    mes "\"Volte quando quiser.\"";
    close;

L_NoDeal:
    mes "[Lobo Solitário]";
    mes "\"Certo, mas você não vai conseguir melhor negócio em qualquer outro lugar!\"";
    close;

L_NoMoney:
    mes "[Lobo Solitário]";
    mes "\"Oh, parece que você não tem dinheiro suficiente.\"";
    close;

L_NoJeans:
    mes "[Lobo Solitário]";
    mes "\"Oh, parece que você não tem bastante Short Jeans.\"";
    close;

L_NoFancy:
    mes "[Lobo Solitário]";
    mes "\"Oh, parece que você não tem bastante Chapéu Fantasia.\"";
    close;

L_NoSkins:
    mes "[Lobo Solitário]";
    mes "\"Oh, parece que você não tem bastante Pele de Cobra da Montanha.\"";
    close;

L_WearingCowboy:
    mes "[Lobo Solitário]";
    mes "\"Ah, vejo que você está usando um chapéu feito com os métodos antigos da minha tribo.\"";
    goto L_Check_Shops;

L_WearingChaps:
    mes "[Lobo Solitário]";
    mes "\"Ah, vejo que você está vestindo calças feitas por minha tribo.\"";
    goto L_Check_Shops;

L_End:
    close;


L_Questpedra:
    mes "[Lobo Solitário]";
    mes "\"How!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Soube que há um índio nesta ilha que sabe fazer uma pedra olho de águia.\"";
    next;
    mes "[Lobo Solitário]";
    mes "\"Minha tribo fazia a pedra olho de águia há muitas gerações para os caçadores da aldeia conseguirem uma visão semelhante à das águias do céu.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Você poderia fazer uma destas pedras para mim?\"";
    next;
    mes "[Lobo Solitário]";
    mes "\"Sim, posso. Mas o poder de ver como as águias do céu só poderia ser dado pelo pajé da tribo. Se eu for fazer a pedra ela não terá poder algum. Quer mesmo assim?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Sim, gostaria que fizesse a pedra",                   L_querpedra,
        "Ah...se é só decorativa, acho que não vou querer...", L_NaoQuero;

L_NaoQuero:
    mes "";
    mes "[Lobo Solitário]";
    mes "\"Só o pajé tinha o poder de chamar o espírito da grande águia para a pedra. Se não quer mais a pedra olho de águia, tudo bem.\"";
    close;

L_querpedra:
    mes "";
    mes "[Lobo Solitário]";
    mes "\"A pedra é feita de um material amarelo-dourado encontrado em algumas árvores. O nome do material é seiva, mas quando endurecido recebe o nome de âmbar. Traga-me 25 pedras de âmbar, 20 tocos de madeira, 20 raízes, 5 garrafas vazias. Além disso vou cobrar 20.000 gp.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Vou até a floresta procurar nas árvores o âmbar.\"";
    goto L_missaoPedraAguia;

L_retornapedra:
    mes "[" + strcharinfo(0) + "]";
    mes "[Lobo Solitário]";
    mes "\"Oh, parece que você realmente quer a pedra olho de águia. Trouxe tudo o que te pedi? São 25 pedras de âmbar, 20 tocos de madeira, 20 raízes, 5 garrafas vazias e 20.000 gp.\"";
    mes "";
    mes "[" + strcharinfo(0) + "]";
    menu
        "Sim, aqui estão",                        L_checagemPedra,
        "Não, ainda falta pegar algumas coisas.", L_AindaNao;

L_AindaNao:
    mes "";
    mes "[Lobo Solitário]";
    mes "\"Então, quando tiver tudo o que lhe pedi, volte e farei a sua pedra.\"";
    goto L_missaoPedraAguia;

L_checagemPedra:
    if (countitem(@pedraAmbar) < 25 || countitem(@tocoMadeira) < 20) goto L_Nao2TemMateriais;
    if (countitem(@Raiz) < 20 || countitem(@GarrafaVazia) < 5) goto L_Nao2TemMateriais;
    if (Zeny < 20000) goto L_Nao2TemGrana;
    if (MOBS_pedraAguia < 200) goto L_Nao2MatouMobs;
    mes "";
    mes "[Lobo Solitário]";
    mes "\"Pois bem, farei a sua pedra imediatamente.\"";
    delitem @pedraAmbar, 25;
    delitem @tocoMadeira, 20;
    delitem @Raiz, 20;
    delitem @GarrafaVazia, 5;
    Zeny = Zeny - 20000;
    QUEST_pedraAguia = 2;
    next;
    goto L_criarPedraAguia;

L_Nao2TemMateriais:
    mes "";
    mes "[Lobo Solitário]";
    mes "\"Você não tem tudo o que eu pedi. Volte quando estiver com tudo.\"";
    goto L_missaoPedraAguia;

L_Nao2TemGrana:
    mes "";
    mes "[Lobo Solitário]";
    mes "\"Você não tem todo o dinheiro que eu pedi. Eu preciso de 20.000 GP.\"";
    goto L_missaoPedraAguia;

L_Nao2MatouMobs:
    mes "É preciso matar pelo menos 200 Troncos Vivos para completar a missão.";
    goto L_missaoPedraAguia;

L_criarPedraAguia:
    mes "Neste momento o índio pega as pedras de âmbar e coloca numa pequena panela, acende uma fogueira, e quando as pedras começam a derreter, ele mistura de forma a ficar um líquido dourado, muito parecido com o mel.";
    next;
    mes "Logo depois, derrama o âmbar derretido num recipiente que mais parece uma bola de vidro. Espera esfriar o âmbar e depois começa a esculpir os detalhes na pedra que agora tem a forma esférica.";
    next;
    mes "[Lobo Solitário]";
    mes "\"Aqui está a sua pedra, faça bom proveito.\"";
    getitem @pedraOlhoAguia, 1;
    mes "";
    mes "* Você ganhou 60.000 pontos de experiência.";
    message strcharinfo(0), "Ganhei 60.000 pontos de experiência.";
    set BaseExp, BaseExp + 60000; // Restrição lvl >= 30
    QUEST_pedraAguia = 0;
    MOBS_pedraAguia = 0;
    DROP_pedraAguia = 0;
    QUEST_MASK1 = QUEST_MASK1 | MASK1_PEDRA_AGUIA;
    close;


L_missaoPedraAguia:
    if (QUEST_pedraAguia != 0) goto L_else;
    QUEST_pedraAguia = 1;
    MOBS_pedraAguia = 0;
    DROP_pedraAguia = 0;
    close;

L_else:
    mes "[Missão: Pedra Olho de Águia]";
    mes "* Itens: " + countitem(@pedraAmbar) + "/25 [" + getitemlink("PedraAmbar") + "]";
    mes "* Itens: " + countitem(@tocoMadeira) + "/20 [" + getitemlink("TocoDeMadeira") + "]";
    mes "* Itens: " + countitem(@Raiz) + "/20 [" + getitemlink("Raiz") + "]";
    mes "* Itens: " + countitem(@GarrafaVazia) + "/5 [" + getitemlink("GarrafaVazia") + "]";
    mes "* Monstros: " + MOBS_pedraAguia + "/200 Troncos Vivos";
    mes "* Recompensa: 60.000 XP";
    // Tronco Vivo: 149 XP, 149x200 ~= 60.000 XP
    // Margem Segurança lvl 51: 30.886 XP
    // Margem Segurança lvl 59: 66.038 XP
    close;

L_Fechar:
    close;
}
