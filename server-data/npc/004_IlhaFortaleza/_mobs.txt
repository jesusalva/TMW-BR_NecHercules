004,47,57,57,74	monster	Limo Alga-Verde	1005,10,0,0,Mob004::On1005
004,49,57,57,78	monster	Limo do Mar	1033,16,0,0,Mob004::On1033
004,119,38,54,25	monster	Cogumelo	1019,6,0,0,Mob004::On1019
004,117,36,55,28	monster	Cogumelo Malígno	1013,8,0,0,Mob004::On1013
004,77,80,21,75	monster	Verme Gigante	1006,6,45000,0,Mob004::On1006
004,82,62,14,22	monster	Verme	1002,5,45000,0,Mob004::On1002
004,112,65,43,22	monster	Trasgo Roxo	1312,6,0,300000,Mob004::On1312
004,109,86,34,67	monster	Trasgo Rosa	1018,10,0,0,Mob004::On1018
004,76,79,21,75	monster	Escorpião Vermelho	1004,8,45000,0,Mob004::On1004
004,118,110,14,21	monster	Trasgo Roxo	1312,4,0,300000,Mob004::On1312
004,59,113,60,12	monster	Escorpião	1003,5,45000,0,Mob004::On1003
004,112,102,0,0	monster	Colméia	1337,1,60000,0,Mob004::On1337_1
004,108,59,0,0	monster	Colméia	1337,1,60000,0,Mob004::On1337_2
004,139,44,0,0	monster	Colméia	1337,1,60000,0,Mob004::On1337_3
004,89,29,0,0	monster	Colméia	1337,1,60000,0,Mob004::On1337_4


004,0,0,0	script	Mob004	NPC32767,{
    end;

On1002:
    @mobId = 1002;
    callfunc "MobPoints";
    callsub S_MOBS_verme;
    end;

On1003:
    @mobId = 1003;
    callfunc "MobPoints";
    callsub S_MOBS_escorpiao;
    callsub S_MOBS_QueimEscorp;
    end;

On1004:
    @mobId = 1004;
    callfunc "MobPoints";
    end;

On1005:
    @mobId = 1005;
    callfunc "MobPoints";
    end;

On1006:
    @mobId = 1006;
    callfunc "MobPoints";
    end;

On1013:
    @mobId = 1013;
    callfunc "MobPoints";
    callsub S_MOBS_cogumelo;
    end;

On1018:
    @mobId = 1018;
    callfunc "MobPoints";
    callsub S_MOBS_trasgo;
    end;

On1019:
    @mobId = 1019;
    callfunc "MobPoints";
    end;

On1033:
    @mobId = 1033;
    callfunc "MobPoints";
    end;

On1312:
    @mobId = 1312;
    callfunc "MobPoints";
    callsub S_MOBS_trasgoRoxo;
    end;

On1337_1:
    @mobId = 1337;
    callfunc "MobPoints";
    @mapa$ = "004";
    @x = 112;
    @y = 102;
    @label$ = "Mob004::On1049_1";
    callfunc "sumonaAbelhas";
    end;
On1049_1:
    @mobId = 1049;
    callfunc "MobPoints";
    callsub S_MOBS_abelha;
    end;

On1337_2:
    @mobId = 1337;
    callfunc "MobPoints";
    @mapa$ = "004";
    @x = 108;
    @y = 59;
    @label$ = "Mob004::On1049_2";
    callfunc "sumonaAbelhas";
    end;
On1049_2:
    @mobId = 1049;
    callfunc "MobPoints";
    callsub S_MOBS_abelha;
    end;

On1337_3:
    @mobId = 1337;
    callfunc "MobPoints";
    @mapa$ = "004";
    @x = 139;
    @y = 44;
    @label$ = "Mob004::On1049_3";
    callfunc "sumonaAbelhas";
    end;
On1049_3:
    @mobId = 1049;
    callfunc "MobPoints";
    callsub S_MOBS_abelha;
    end;

On1337_4:
    @mobId = 1337;
    callfunc "MobPoints";
    @mapa$ = "004";
    @x = 89;
    @y = 29;
    @label$ = "Mob004::On1049_4";
    callfunc "sumonaAbelhas";
    end;
On1049_4:
    @mobId = 1049;
    callfunc "MobPoints";
    callsub S_MOBS_abelha;
    end;


S_MOBS_abelha:
    if (QUEST_ursinho != 1 && QUEST_ursinho != 2) goto L_Return;
    @max = 1125;
    @mobs = QUEST_ursinhoAbelhas;
    @flag = @QUEST_ursinhoAbelhas;
    callfunc "mobContagem";
    QUEST_ursinhoAbelhas = @mobs;
    @QUEST_ursinhoAbelhas = @flag;
    return;

S_MOBS_cogumelo:
    if (QUEST_cogumelo != 2) goto L_Return;
    @max = 40;
    @mobs = MOBS_cogumelo;
    @flag = @MOBS_cogumelo;
    callfunc "mobContagem";
    MOBS_cogumelo = @mobs;
    @MOBS_cogumelo = @flag;
    return;

S_MOBS_escorpiao:
    if (QUEST_praia != 12) goto L_Return;
    @max = 30;
    @mobs = QUEST_contamobs;
    @flag = @QUEST_contamobs;
    callfunc "mobContagem";
    QUEST_contamobs = @mobs;
    @QUEST_contamobs = @flag;
    return;

S_MOBS_QueimEscorp:
    if (QUEST_pocaoQueimadura != 2) goto L_Return;
    @max = 20;
    @mobs = MOBS_queimaduraEscorpiao;
    @flag = @MOBS_queimaduraEscorpiao;
    callfunc "mobContagem";
    MOBS_queimaduraEscorpiao = @mobs;
    @MOBS_queimaduraEscorpiao = @flag;
    return;

S_MOBS_trasgo:
    if (QUEST_trasgo != 8) goto L_Return;
    @max = 35;
    @mobs = MOBS_trasgo;
    @flag = @MOBS_trasgo;
    callfunc "mobContagem";
    MOBS_trasgo = @mobs;
    @MOBS_trasgo = @flag;
    return;

S_MOBS_trasgoRoxo:
    if (QUEST_trasgo != 3) goto L_Return;
    @mapa$ = "004";
    @label$ = "Mob004::On1312";
    callfunc "mobExterminio";
    if (@flag == 1) set QUEST_trasgo, 4;
    return;

S_MOBS_verme:
    if (QUEST_praia != 7) goto L_Return;
    @max = 15;
    @mobs = QUEST_contamobs;
    @flag = @QUEST_contamobs;
    callfunc "mobContagem";
    QUEST_contamobs = @mobs;
    @QUEST_contamobs = @flag;
    return;

L_Return:
    return;
}
