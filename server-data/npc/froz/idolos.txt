froz,94,41,0	script	Estátua#1	NPC605,{
    //set $@FrozID, getcharid(3);
    //donpcevent "AdmGuerra::OnSurgeReiGelido";
    @numero = 1;
    callfunc "funcEstatuasDoReiGelido";
    close;
}


froz,94,41,0	script	AdmEstatua1	NPC32767,{
    end;

OnArrebentar:
    $Froz_EstatuasArrebentadas = $Froz_EstatuasArrebentadas + 1;
    if ($Froz_EstatuasArrebentadas < $MaxEstatuasFroz)  announce $Froz_EstatuasArrebentadas+"/"+$MaxEstatuasFroz+" estátuas do Rei Gélido foram destruidas em Froz!", 0;
    if ($Froz_EstatuasArrebentadas >= $MaxEstatuasFroz) donpcevent "AdmGuerra::OnSurgeReiGelido";
    end;

OnPetDeath:
    @focoX = 94;
    @focoY = 41;
    @focoRaio = 10;
    @foco = 1;
    @inFront = getareausers("froz", @focoX-@focoRaio, @focoY-@focoRaio, @focoX+@focoRaio, @focoY+@focoRaio);
    @mobFront = mobcount("froz", "AdmEstatua"+@foco+"::OnPetDeath");
    areamonster "froz", @focoX-@focoRaio, @focoY-@focoRaio, @focoX+@focoRaio, @focoY+@focoRaio, "", 1378, ((@inFront*3)-@mobFront), "AdmEstatua"+@foco+"::OnPetDeath"; //Sumona dentro do reino inteiro!
    end;
}


froz,31,28,0	script	Estátua#2	NPC605,{
    @numero = 2;
    callfunc "funcEstatuasDoReiGelido";
    close;
}

froz,31,28,0	script	AdmEstatua2	NPC32767,{
    end;

OnArrebentar:
    $Froz_EstatuasArrebentadas = $Froz_EstatuasArrebentadas + 1;
    if ($Froz_EstatuasArrebentadas < $MaxEstatuasFroz)  announce $Froz_EstatuasArrebentadas+"/"+$MaxEstatuasFroz+" estátuas do Rei Gélido foram destruidas em Froz!", 0;
    if ($Froz_EstatuasArrebentadas >= $MaxEstatuasFroz) donpcevent "AdmGuerra::OnSurgeReiGelido";
    end;

OnPetDeath:
    @focoX = 31;
    @focoY = 28;
    @focoRaio = 10;
    @foco = 2;
    @inFront = getareausers("froz", @focoX-@focoRaio, @focoY-@focoRaio, @focoX+@focoRaio, @focoY+@focoRaio);
    @mobFront = mobcount("froz", "AdmEstatua"+@foco+"::OnPetDeath");
    areamonster "froz", @focoX-@focoRaio, @focoY-@focoRaio, @focoX+@focoRaio, @focoY+@focoRaio, "", 1378, ((@inFront*3)-@mobFront), "AdmEstatua"+@foco+"::OnPetDeath"; //Sumona dentro do reino inteiro!
    end;
}


froz,150,45,0	script	Estátua#3	NPC605,{
    @numero = 3;
    callfunc "funcEstatuasDoReiGelido";
    close;
}

froz,150,45,0	script	AdmEstatua3	NPC32767,{
    end;

OnArrebentar:
    $Froz_EstatuasArrebentadas = $Froz_EstatuasArrebentadas + 1;
    if ($Froz_EstatuasArrebentadas < $MaxEstatuasFroz)  announce $Froz_EstatuasArrebentadas+"/"+$MaxEstatuasFroz+" estátuas do Rei Gélido foram destruidas em Froz!", 0;
    if ($Froz_EstatuasArrebentadas >= $MaxEstatuasFroz) donpcevent "AdmGuerra::OnSurgeReiGelido";
    end;

OnPetDeath:
    @focoX = 150;
    @focoY = 45;
    @focoRaio = 10;
    @foco = 3;
    @inFront = getareausers("froz", @focoX-@focoRaio, @focoY-@focoRaio, @focoX+@focoRaio, @focoY+@focoRaio);
    @mobFront = mobcount("froz", "AdmEstatua"+@foco+"::OnPetDeath");
    areamonster "froz", @focoX-@focoRaio, @focoY-@focoRaio, @focoX+@focoRaio, @focoY+@focoRaio, "", 1378, ((@inFront*3)-@mobFront), "AdmEstatua"+@foco+"::OnPetDeath"; //Sumona dentro do reino inteiro!
    end;
}

froz,41,58,0	script	Estátua#4	NPC605,{
    @numero = 4;
    callfunc "funcEstatuasDoReiGelido";
    close;
}

froz,41,58,0	script	AdmEstatua4	NPC32767,{
    end;

OnArrebentar:
    $Froz_EstatuasArrebentadas = $Froz_EstatuasArrebentadas + 1;
    if ($Froz_EstatuasArrebentadas < $MaxEstatuasFroz)  announce $Froz_EstatuasArrebentadas+"/"+$MaxEstatuasFroz+" estátuas do Rei Gélido foram destruidas em Froz!", 0;
    if ($Froz_EstatuasArrebentadas >= $MaxEstatuasFroz) donpcevent "AdmGuerra::OnSurgeReiGelido";
    end;

OnPetDeath:
    @focoX = 41;
    @focoY = 58;
    @focoRaio = 10;
    @foco = 4;
    @inFront = getareausers("froz", @focoX-@focoRaio, @focoY-@focoRaio, @focoX+@focoRaio, @focoY+@focoRaio);
    @mobFront = mobcount("froz", "AdmEstatua"+@foco+"::OnPetDeath");
    areamonster "froz", @focoX-@focoRaio, @focoY-@focoRaio, @focoX+@focoRaio, @focoY+@focoRaio, "", 1378, ((@inFront*3)-@mobFront), "AdmEstatua"+@foco+"::OnPetDeath"; //Sumona dentro do reino inteiro!
    end;
}

froz,131,28,0	script	Estátua#5	NPC605,{
    @numero = 5;
    callfunc "funcEstatuasDoReiGelido";
    close;
}

froz,131,28,0	script	AdmEstatua5	NPC32767,{
    end;

OnArrebentar:
    $Froz_EstatuasArrebentadas = $Froz_EstatuasArrebentadas + 1;
    if ($Froz_EstatuasArrebentadas < $MaxEstatuasFroz)  announce $Froz_EstatuasArrebentadas+"/"+$MaxEstatuasFroz+" estátuas do Rei Gélido foram destruidas em Froz!", 0;
    if ($Froz_EstatuasArrebentadas >= $MaxEstatuasFroz) donpcevent "AdmGuerra::OnSurgeReiGelido";
    end;

OnPetDeath:
    @focoX = 131;
    @focoY = 28;
    @focoRaio = 10;
    @foco = 5;
    @inFront = getareausers("froz", @focoX-@focoRaio, @focoY-@focoRaio, @focoX+@focoRaio, @focoY+@focoRaio);
    @mobFront = mobcount("froz", "AdmEstatua"+@foco+"::OnPetDeath");
    areamonster "froz", @focoX-@focoRaio, @focoY-@focoRaio, @focoX+@focoRaio, @focoY+@focoRaio, "", 1378, ((@inFront*3)-@mobFront), "AdmEstatua"+@foco+"::OnPetDeath"; //Sumona dentro do reino inteiro!
    end;
}


function	script	funcEstatuasDoReiGelido	{
    $MaxEstatuasFroz = 5;
    goto L_Menu;

L_Menu:
    mes "[" + strcharinfo(0) + "]";
    menu
        "Quero ler a placa!",     L_Leitura,
        "Quero quebrar estátua!", L_IniciarAtaque,
        "Nada! Deixa quieto!",    L_Fechar;

L_Leitura:
    mes "";
    mes "[Estátua do Rei Gélido]";
    mes "\"Mais corajoso que um leão, mais forte que um urso, mais rápido que um falcão, e mais inteligente que uma rapoza.\"";
    next;
    mes "Esta estátua é um tributo ao magnânimo Rei Gélido. Cuja história ficará marcada para sempre nos corações dos cidadãos de Froz.";
    next;
    goto L_Menu;

L_IniciarAtaque:
    if ($PosseDeFroz != 0 && Reino == $PosseDeFroz) goto L_MensagemAoTraidor;
    if (Reino == 0) goto L_MensagemAoHeremita;
    if (gettime(4) != $DiaDeVisitacoes || gettime(3) < $HoraDeGuerra || gettime(3) > $HoraDeGuerra + 3) goto L_FalhaDeMomento; //← desabilite essa linha para invadir todos dos dias!
    if ($Froz_InvasaoPermitida != 1) goto L_FalhaAoArrebentar;
    disablenpc "Estátua#" + @numero;
    pvpon "froz";
    //pvpon "froz001";
    //pvpon "froz002";
    announce "A estátua nº" + @numero + " do Rei Gélido está sendo atacada!", 0;
    if (@numero == 1) goto L_CalcDefesa1;
    if (@numero == 2) goto L_CalcDefesa2;
    if (@numero == 3) goto L_CalcDefesa3;
    if (@numero == 4) goto L_CalcDefesa4;
    if (@numero == 5) goto L_CalcDefesa5;
    return;

L_MensagemAoTraidor:
    mes "";
    mes "[" + strcharinfo(0) + "]";
    mes "\"Mas seria tolice ir contra o meu próprio reino!\"";
    return;

L_MensagemAoHeremita:
    mes "";
    mes "[" + strcharinfo(0) + "]";
    mes "\"Mas não posso, pois sou apenas um heremita que não é cidadão de algum reino!\"";
    return;

L_FalhaDeMomento:
    @Valor = $HoraDeGuerra;
    callfunc "getConvHora";
    @Valor = $DiaDeVisitacoes;
    callfunc "getConvDiaTexto";
    mes "";
    mes "[" + strcharinfo(0) + "]";
    mes "=D \"Mas vou arrebentá-la às " + @RealHora + "h de " + @RealDia$ + "!\"";
    return;

L_FalhaAoArrebentar:
    mes "";
    mes "[" + strcharinfo(0) + "]";
    mes "=S \"Mas infelizmente está protigida magicamente!\"";
    return;

L_CalcDefesa1:
    @focoX = 94;
    @focoY = 41;
    goto L_IniciaDefesa;

L_CalcDefesa2:
    @focoX = 31;
    @focoY = 28;
    goto L_IniciaDefesa;

L_CalcDefesa3:
    @focoX = 150;
    @focoY = 45;
    goto L_IniciaDefesa;

L_CalcDefesa4:
    @focoX = 41;
    @focoY = 58;
    goto L_IniciaDefesa;

L_CalcDefesa5:
    @focoX = 131;
    @focoY = 28;
    goto L_IniciaDefesa;

L_IniciaDefesa:
    @focoRaio = 10;
    @inFront = getareausers("froz", @focoX-@focoRaio, @focoY-@focoRaio, @focoX+@focoRaio, @focoY+@focoRaio);
    monster "froz", @focoX, @focoY, "", 1379, 1, "AdmEstatua" + @numero + "::OnArrebentar";
    areamonster "froz", @focoX-@focoRaio, @focoY-@focoRaio, @focoX+@focoRaio, @focoY+@focoRaio, "", 1378, (@inFront*3), "AdmEstatua" + @numero + "::OnPetDeath"; // Sumona dentro do reino inteiro!
    close2;
    return;

L_Fechar:
    return;
}


froz,0,0,0	script	AdmGuerra	NPC32767,{
    end;

OnSurgeReiGelido:
    //attachrid($@FrozID);
    announce "O próprio Rei Gélido saiu do castelo para enfrentar os invasores em Froz!", 0;
    monster "froz", 98, 21, "", 1380, 1, "AdmGuerra::OnGelidoDeath";
    end;

OnGelidoDeath:
    if (Reino == 0) set @NomeReino$, "lugares desconhecidos";
    if (Reino == 1) set @NomeReino$, "Bhramir";
    if (Reino == 2) set @NomeReino$, "Halicarnazo";
    if (Reino == 3) set @NomeReino$, "Froz";
    if ($PosseDeFroz != 0 && Reino == $PosseDeFroz) goto L_seTraidor; // ← Caso o jogador mate o seu Próprio Rei!
    
    enablenpc "Estátua#1";
    enablenpc "Estátua#2";
    enablenpc "Estátua#3";
    enablenpc "Estátua#4";
    enablenpc "Estátua#5";
    set $Froz_EstatuasArrebentadas, 0; // ← Volta ao incício
    pvpoff "froz";
    //pvpoff "froz001";
    //pvpoff "froz002";
    enablenpc "#FrozPorta1Fechada";
    disablenpc "#FrozPorta1Aberta";
    killmonster "froz", "ArrebentarFroz";
    killmonster "froz", "ArrebentarFroz::OnPetDeath1";
    killmonster "froz", "ArrebentarFroz::OnPetDeath2";
    killmonster "froz", "AdmEstatua1::OnArrebentar";
    killmonster "froz", "AdmEstatua1::OnPetDeath";
    killmonster "froz", "AdmEstatua2::OnArrebentar";
    killmonster "froz", "AdmEstatua2::OnPetDeath";
    killmonster "froz", "AdmEstatua3::OnArrebentar";
    killmonster "froz", "AdmEstatua3::OnPetDeath";
    killmonster "froz", "AdmEstatua4::OnArrebentar";
    killmonster "froz", "AdmEstatua4::OnPetDeath";
    killmonster "froz", "AdmEstatua5::OnArrebentar";
    killmonster "froz", "AdmEstatua5::OnPetDeath";

    announce "##3Froz foi conquistada pelo povo do Reino de " + @NomeReino$ + "!", 0;
    $PosseDeFroz = Reino;
    areawarp "froz", 21, 20, 175, 66, "froz", 93, 79;
    areawarp "froz001", 0, 0, 199, 199, "froz", 93, 79;
    areawarp "froz002", 0, 0, 199, 199, "froz", 93, 79;
    misceffect 2; // ← Só sairá se for perto do deste NPC
    end;

L_seTraidor:
    announce strcharinfo(0)+" está traindo o próprio rei em " + @NomeReino$ + "!", 0;
    monster "froz", 98, 21, "", 1380, 1, "AdmGuerra::OnGelidoDeath";
    end;
}
