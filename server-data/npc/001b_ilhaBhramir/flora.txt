


001b,80,88,0	script	Flora	NPC384,{
    if (BaseLevel < 18) goto L_Falas;

    if (QUEST_pocaoQueimadura == 0) goto L_Falas;
    if (QUEST_pocaoQueimadura == 1) goto L_perguntarPocao1;
    if (QUEST_pocaoQueimadura == 2) goto L_checarItens1;
    if (QUEST_pocaoQueimadura == 3) goto L_receberPocao;
    if (QUEST_pocaoQueimadura <= 5) goto L_vaLogo;
    goto L_Falas;

L_Falas:
    mes "[Flora]";
    mes "Você encontra com uma jovem senhora em um profundo estado de meditação e não resiste em fazer uma pergunta.";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"O que a senhora está fazendo?\"";
    next;
    mes "[Flora]";
    mes "\"Olá jovem. Estou apenas colhendo algumas ervas medicinais e também meditando um pouco.\"";
    next;
    mes "[Flora]";
    mes "\"Eu sou boticária da Vila dos Pescadores. Faço poções de todos os tipos utilizando ervas medicinais.";
    mes "E este local possui muitas das ervas que utilizo em minhas poções.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    if (Sex == 0) mes "\"Entendo. Obrigada pela informação.\"";
    if (Sex == 1) mes "\"Entendo. Obrigado pela informação.\"";
    close;

L_perguntarPocao1:
    mes "[Flora]";
    mes "Você encontra uma jovem senhora que parece estar colhendo ervas na encosta da montanha.";
    mes "Esta pode ser Flora.";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"A senhora é Flora? A boticária da Vila dos Pescadores?\"";
    next;
    mes "[Flora]";
    mes "\"Sim, sou eu. Está precisando de alguma poção?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Sim, uma poção para queimaduras, você tem?\"";
    next;
    mes "[Flora]";
    mes "\"Logo essa... não tenho mais nenhuma, me desculpe...\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Tudo bem, tchau...", L_Fechar,
        "Eu estou realmente precisando dela.", L_perguntarPocao2;

L_perguntarPocao2:
    mes "[" + strcharinfo(0) + "]";
    mes "\"Uma amiga minha está morrendo de dor, não tem algum jeito?\"";
    next;
    mes "[Flora]";
    mes "\"Até tem um jeito sim... você pode pegar os ingredientes para que eu poça fazer a poção. E também pagar por meus serviços...\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Nossa, eu vou pegar tudo e você ainda vai cobrar... Aff...\"";
    next;
    mes "[Flora]";
    mes "\"Você disse que sua amiga está morrendo de dor. Então irá me ajudar com os ingredientes para aliviar sua dor o quanto antes.\"";
    next;
    mes "[Flora]";
    mes "\"E também, negócios são negócios... Mas irei cobrar apenas metade do valor.";
    mes "O que vai lhe custar apenas 1.500 GP.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "De jeito nenhum.",         L_Fechar,
        "Tudo bem, o que precisa?", L_ingredientes,
        "Não tenho esta quantia.",  L_Fechar;

L_checarItens1:
    mes "[Flora]";
    mes "\"Trouxe todos os itens para a poção?\"";
    menu
        "Sim. Todos.",                 L_checarItens2,
        "Não. Pode repetir os itens?", L_ingredientes;

L_checarItens2:
    @cocoVerde = 3284;
    @cascoTartaruga = 3403;
    @peleTartaruga = 3402;
    @ferraoEscorpiao = 507;
    @GosmaVerme = 505;

    if (countitem(@cocoVerde) < 1) goto L_Insuficiente1;
    if (countitem(@cascoTartaruga) < 1) goto L_Insuficiente1;
    if (countitem(@peleTartaruga) < 2) goto L_Insuficiente1;
    if (countitem(@ferraoEscorpiao) < 2) goto L_Insuficiente1;
    if (countitem(@GosmaVerme) < 5) goto L_Insuficiente1;
    if (Zeny < 1500) goto L_Insuficiente2;
    if (MOBS_queimaduraEscorpiao < 20 || MOBS_queimaduraTartaruga < 20) goto L_Insuficiente3;
    mes "[Flora]";
    mes "\"Isso mesmo, está tudinho aqui... Agora sim poderei fazer a poção.\"";
    delitem @cocoVerde, 1;
    delitem @cascoTartaruga, 1;
    delitem @peleTartaruga, 2;
    delitem @ferraoEscorpiao, 2;
    delitem @GosmaVerme, 5;
    Zeny = Zeny - 1500;
    QUEST_pocaoQueimadura = 3;
    next;
    mes "[Flora]";
    mes "\"Agora espere enquanto preparo a poção... é rapidinho.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Está bem, estarei aqui esperando.\"";
    close;

L_Insuficiente1:
    mes "[Flora]";
    mes "\"Você não tem todos os ingredientes para a minha poção.\"";
    next;
    goto L_ingredientes;

L_ingredientes:
    mes "[Flora]";
    mes "\"Para essa poção vou precisar de 1 [" + getitemlink("CocoVerde") + "], 1 [" + getitemlink("CascoTartaruga") + "], 2 pedacinhos de [" + getitemlink("PeleTartaruga") + "], 2 [" + getitemlink("FerraoDeEscorpiao") + "] e 5 [" + getitemlink("GosmaDeVerme") + "].\"";
    next;
    mes "[Flora]";
    mes "\"Estou te esperando, traga tudo e mais 1.500 GP.\"";
    mes "";
    goto L_MissPotQueim;

L_Insuficiente2:
    mes "[Flora]";
    mes "\"O combinado era de 1.500 GP. E você não tem esta quantia.";
    mes "Estarei esperando que me traga toda a quantia para que eu poça fazer a poção.\"";
    mes "";
    goto L_MissPotQueim;

L_Insuficiente3:
    mes "A quantidade de monstros é insuficiente.";
    mes "";
    goto L_MissPotQueim;

L_receberPocao:
    mes "[" + strcharinfo(0) + "]";
    mes "\"E então?\"";
    next;
    getinventorylist;
    if (@inventorylist_count == 100) goto L_Cheio;
    mes "[Flora]";
    mes "\"A poção está pronta... Pegue aqui...\"";
    mes "";
    getitem "PocaoQueimadura", 1;
    QUEST_pocaoQueimadura = 4;

    mes "* Você ganhou 2.800 pontos de experiência.";
    message strcharinfo(0), "Ganhei 2.800 pontos de experiência.";
    BaseExp = BaseExp + 2800;

    MOBS_queimaduraEscorpiao = 0;
    @MOBS_queimaduraEscorpiao = 0;
    MOBS_queimaduraTartaruga = 0;
    @MOBS_queimaduraTartaruga = 0;
    next;
    mes "[" + strcharinfo(0) + "]";
    if (Sex == 0) mes "\"Muito obrigada, agora vou levar esta poção para minha amiga...\"";
    if (Sex == 1) mes "\"Muito obrigado, agora vou levar esta poção para minha amiga...\"";
    close;

L_Cheio:
    mes "[Flora]";
    mes "\"Você não tem espaço para a minha poção.";
    mes "Estarei esperando você voltar com mais espaço.\"";
    close;

L_vaLogo:
    mes "[Flora]";
    mes "\"O que ainda faz aqui?!";
    mes "Leve logo a Poção de Queimadura para a sua amiga.\"";
    close;

L_MissPotQueim:
    if (QUEST_pocaoQueimadura == 2) goto L_pular;
    QUEST_pocaoQueimadura = 2;
    MOBS_queimaduraEscorpiao = 0;
    @MOBS_queimaduraEscorpiao = 0;
    MOBS_queimaduraTartaruga = 0;
    @MOBS_queimaduraTartaruga = 0;
    goto L_pular;

L_pular:
    mes "[Missão: Poção para Queimadura]";
    mes "* Monstros: " + MOBS_queimaduraEscorpiao + "/20 Escorpiões";
    mes "* Monstros: " + MOBS_queimaduraTartaruga + "/20 Tartarugas";
    // Escorpião: 14XP, 14x20 = 2.80 XP
    // Tartaruga: 55XP, 14x20 = 1.100 XP
    //                        = 1.380 x 2 = 2.760 XP
    // RECOMPENSA >> 2.800 XP
    // LIMITADA PARA LVL 18.
    // FINALIZA COM LVL 20 e 92,5%
    close;

L_Fechar:
    close;
}
