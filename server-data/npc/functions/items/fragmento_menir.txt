function	script	useFragmentoMenir	{
    // Locais probidos
    if (isin("prisao", 0, 0, 59, 50) == 1) goto L_preso;
    if (isin("005-1", 0, 0, 111, 99) == 1) goto L_quartel;
    if (@Energizado) goto L_Warp2;

    // Se TICK_fragmentoMenir == 0 segue em frente. Na primeira vez não precisa energizar.
    if (TICK_fragmentoMenir + 72 * 60 * 60 > gettimetick(2)) goto L_Nao2Energizado; // espera de 72 horas.

    // Transportando para o ultimo savepoint.
    TICK_fragmentoMenir = gettimetick(2);
    specialeffect2 311;
    goto L_Warp2;

L_Warp2:
    if (isin("002", 24, 39, 88, 113) == 1) goto L_Warp2002_1;
    if (isin("002", 111, 39, 175, 113) == 1) goto L_Warp2002_2;
    warp getsavepoint(0), getsavepoint(1), getsavepoint(2);
    specialeffect2 310;
    @Energizado = 0;
    return;

L_Nao2Energizado:
    @n = 71 - (gettimetick(2) - TICK_fragmentoMenir) / (60 * 60);
    if (@n >= 2) message strcharinfo(0), "* Faltam mais de " + @n + "hs para que um fragmento da menir seja energizado.";
    if (@n == 1) message strcharinfo(0), "* Falta mais de 1h para que um fragmento da menir seja energizado.";
    if (@n == 0) message strcharinfo(0), "* Falta menos de 1h para que um fragmento da menir seja energizado.";
    message strcharinfo(0), "¬_¬ Não funcionou!";
    @n = 0;
    return;

L_preso:
    message strcharinfo(0), "* Nem pense nisto! Você está na prisão.";
    message strcharinfo(0), "¬_¬ Não funcionou!";
    return;

L_quartel:
    message strcharinfo(0), "* Não tente trapassear! Logo aqui! O Quartel General.";
    message strcharinfo(0), "¬_¬ Não funcionou!";
    return;

L_Warp2002_1:
    warp "002", 91, 93;
    specialeffect2 310;
    message strcharinfo(0), "" + strcharinfo(0) + " saiu do desafio do Rei Stone.";
    return;

L_Warp2002_2:
    warp "002", 108, 93;
    specialeffect2 310;
    message strcharinfo(0), "" + strcharinfo(0) + " saiu do desafio do Rei Gélido.";
    return;
}
