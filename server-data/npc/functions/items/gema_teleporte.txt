function	script	useGemaTeleporte	{
    if (isin("013", 0, 0, 299, 299)) goto L_Return;

    specialeffect2 311;
    warp "013", 0, 0;
    specialeffect2 310;
    return;

L_Return:
    message strcharinfo(0), "Não funcionou...";
    getitem "GemaTeleporte", 1;
    return;
}
