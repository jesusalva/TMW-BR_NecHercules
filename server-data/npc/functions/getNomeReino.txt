function	script	getNomeReino	{
    if (Reino == 0 && $PoderHalicarnazo >= $PoderFroz) goto L_ParaFroz;
    if (Reino == 0 && $PoderHalicarnazo < $PoderFroz ) goto L_ParaHali;
    goto L_showNomeReino;

L_ParaFroz:
    $PoderFroz = $PoderFroz + BaseLevel;
    Reino = 3;
    savepoint "froz001", 104, 53;
    goto L_showNomeReino;

L_ParaHali:
    $PoderHalicarnazo = $PoderHalicarnazo + BaseLevel;
    Reino = 2;
    savepoint "hali001", 134, 93;
    goto L_showNomeReino;

L_showNomeReino:
    if (Reino == 0) set @NomeReino$, l("lugares desconhecidos");
    if (Reino == 1) set @NomeReino$, l("Bhramir");
    if (Reino == 2) set @NomeReino$, l("Halicarnazo");
    if (Reino == 3) set @NomeReino$, l("Froz");
    return;
}
