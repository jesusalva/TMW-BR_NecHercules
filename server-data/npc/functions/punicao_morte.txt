
function	script	punicaoMorte	{
    if (BaseLevel < 10) goto L_Return;
    if (ZONA_SEGURA == 1) goto L_zonaSegura;
    if (PC_DIE_COUNTER >= 1 && PC_DIE_COUNTER2 == 0) goto L_aviso;
    if (PC_DIE_COUNTER != PC_DIE_COUNTER2) goto L_perderXP;
    return;

L_perderXP:
    @base = 9 - (BaseLevel/10);
    @base = @base * 5 + 20;
    @xp = (NextBaseExp * @base) / 1000;
    if (@xp > BaseExp) set @xp, BaseExp;
    BaseExp = BaseExp - @xp;
    PC_DIE_COUNTER2 = PC_DIE_COUNTER;
    if (@xp > 0) message strcharinfo(0), "Eu perdi " + @xp + " pontos de experiência.";
    return;

L_aviso:
    mes ".:: AVISO ::.";
    mes "Ao morrer você perderá de 6% a 2% de seus pontos de experiência dependendo de seu level:";
    mes "";
    mes "* lvl 10..19: 6,0%";
    mes "* lvl 20..29: 5,5%";
    mes "* lvl 30..39: 5,0%";
    mes "* lvl 40..49: 4,5%";
    mes "* lvl 50..59: 4,0%";
    mes "* lvl 60..69: 3,5%";
    mes "* lvl 70..79: 3,0%";
    mes "* lvl 80..89: 2,5%";
    mes "* lvl 90..98: 2,0%";
    next;
    mes "Seja mais cuidadoso em suas aventuras!!!";
    PC_DIE_COUNTER2 = PC_DIE_COUNTER;
    close2;
    return;

L_zonaSegura:
    ZONA_SEGURA = 0;
    PC_DIE_COUNTER2 = PC_DIE_COUNTER;
    return;

L_Return:
    return;
}
