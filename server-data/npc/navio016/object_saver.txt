navio016,60,23,0	script	Baú#navio016	NPC111,{
    @npc$ = "Baú";
    @map$ = "navio016";
    @x = 60;
    @y = 24;
    callfunc "objectSaver";
    close;
}

navio016,60,24,0	script	#Pena-navio016	NPC32767,1,1,{
    callfunc "limparDist";
    end;
}

navio016,60,24,0	script	#Zona-navio016	NPC32767,1,1,{
    if (ZONA_SEGURA == 1) message strcharinfo(0), "Estou fora da zona segura.";
    set ZONA_SEGURA, 0; // Na saída do PVP a zona deixa de ser segura.
    close;
}
