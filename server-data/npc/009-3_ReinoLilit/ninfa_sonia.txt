


009-3,141,48,0	script	Sonia	NPC316,{
    if (Sex == 1) set @SexForasteirx$, "forasteiro";
    if (Sex != 1) set @SexForasteirx$, "forasteira";

    if (SeCacadorDeNecromantes != 1) goto L_comprimenta;
    if (NinfaSonia_capitulo == 1) goto L_Retorno;
    if (NinfaSonia_capitulo == 2) goto L_buscar;

    if (NinfaSonia_capitulo == 3 && Zeny < 50000)  goto L_sem_grana_novamente;
    if (NinfaSonia_capitulo == 3 && Zeny >= 50000) goto L_ComGrana;

    if (NinfaSonia_capitulo == 4) goto L_CumprimentaSabe;
    goto L_Inicio;

L_comprimenta:
    mes "[Ninfa Sônia]";
    mes "\"Olá " + @SexForasteirx$ + "!\"";
    close;

L_CumprimentaSabe:
    mes "[Ninfa Sônia]";
    mes "\"Olá " + strcharinfo(0) + "!\"";
    next;
    mes "[Ninfa Sônia]";
    mes "\"Quer fazer outra Couraça de Pele de Cobra?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "=D Sim! Preciso de outra.",       L_recomeco,
        "¬¬ Não me interessa no momento.", L_Tchau;

L_Tchau:
    mes "[Ninfa Sônia]";
    mes "\"Ok! Até a próxima!\"";
    close;

L_recomeco:
    NinfaSonia_capitulo = 1;
    goto L_selecao;

L_Inicio:
    mes "[Ninfa Sônia]";
    mes "\"Olá " + @SexForasteirx$ + "!";
    mes "Posso te ajudar de alguma forma?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Meu nome não é \"" + @SexForasteirx$ + "\"! Eu me chamo \"" + strcharinfo(0) + "\"!\"";
    next;
    mes "[Ninfa Sônia]";
    mes "\"Ok, " + strcharinfo(0) + "!";
    mes "Meu nome é Sônia.";
    mes "Você quer que eu te faça alguma roupa?";
    mes "Sei fazer ótimas roupas de couro.\"";
    next;
    menu
        "Se for igual a sua pode esquecer!",            L_Fechar,
        "Pode me fazer um [Couraça de Pele de Cobra]?", L_queroCouraca,
        "Roupas de couro não me interessam.",           L_Fechar;

L_queroCouraca:
    mes "[Ninfa Sônia]";
    mes "\"Sei sim! Me traga esta listinha de itens que eu te darei a melhor couraça que você já viu na face de Mana.\"";
    next;
    mes "[LISTA PARA A COURAÇA]";
    mes "* 100 Casulos de Bicho-da-Seda";
    mes "* 050 Couros de Cobra da Montanha";
    mes "[PREÇO: 50.000 GP]";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "O.o Caramba! Tudo isso?";
    next;
    mes "[Ninfa Sônia]";
    mes "\"Item raros são feitos de materiais difíceis de se conseguir.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "Ok Sônia! Então vou buscar esses itens...";
    NinfaSonia_capitulo = 1;
    close;

L_Retorno:
    mes "[Ninfa Sônia]";
    mes "\"Olá " + strcharinfo(0) + "!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "Olá Sônia!";
    next;
    goto L_selecao;

L_selecao:
    mes "[" + strcharinfo(0) + "]";
    menu
        "=S Me mostra novamente a lista de itens.", L_Lista2,
        "=D Eu já consegui todos os itens.", L_checaitens,
        "¬.¬ Mas Deixe para depois, então!", L_Fechar;

L_Lista2:
    mes "[LISTA PARA A COURAÇA]";
    mes "+ 100 Casulos de Bicho-da-Seda";
    mes "+ 050 Couros de Cobra da Montanha";
    mes "[PREÇO: 50.000 GP]";
    next;
    goto L_selecao;

L_checaitens:
    if (countitem(718)<100) goto L_sem_casulo;
    if (countitem(641)<50) goto L_sem_couro;
    mes "[Ninfa Sônia]";
    mes "\"Deixe eu ver!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "Tome!";
    next;
    mes "[Ninfa Sônia]";
    mes "\"É realmente você tem tudo o que precisa.\"";
    next;
    mes "[Ninfa Sônia]";
    mes "\"Vai querer deixar comigo para fazer sua Couraça?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "=D Deixo sim!",      L_deixo,
        "=S Não, agora não!", L_Tchau2;

L_Tchau2:
    mes "[Ninfa Sônia]";
    mes "\"Ok! Então até mais!\"";
    close;

L_deixo:
    if (countitem(718) < 100) goto L_sem_casulo; // garantia que o jogador não retirou os itens
    if (countitem(641) < 50) goto L_sem_couro;   // "
    NinfaSonia_tempo = gettimetick(2);
    NinfaSonia_capitulo = 2;
    delitem 641, 50;
    delitem 718, 100;
    // 641=couros de cobra da Montanha
    // 718=Casulos de Bicho-da-Seda
    mes "[Ninfa Sônia]";
    mes "\"Ok! Volte daqui a uma hora!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "O.o Daqui a uma hora ainda?";
    next;
    mes "[Ninfa Sônia]";
    mes "\"Sim! Ou você quer que eu faça de qualquer jeito?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "Não! Pode deixar eu espero.";
    mes "Até mais!";
    close;

L_buscar:
    //mes @time +" - " + NinfaSonia_tempo + " = "+ (@time-NinfaSonia_tempo);
    //mes @time +" - " + NinfaSonia_tempo + " = "+ (@time-NinfaSonia_tempo);
    if (gettimetick(2) > NinfaSonia_tempo + (60 * 60)) goto L_feito;
    mes "[" + strcharinfo(0) + "]";
    mes "Olá Sônia!";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "Fez minha Couraça?";
    next;
    mes "[Ninfa Sônia]";
    mes "\"Claro que não!\"";
    next;
    mes "[Ninfa Sônia]";
    mes "\"Volte daqui a " + (((NinfaSonia_tempo + (60 * 60)) - gettimetick(2)) / 60) +" minutos que eu te entrego!\"";
    next;
    mes "[Ninfa Sônia]";
    mes "\"Mas não se esqueça de meu pagamento!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "Ok! Eu trago o dinheiro!";
    close;

L_feito:
    mes "[" + strcharinfo(0) + "]";
    mes "Sônia, eu não acredito que você ainda não fez minha couraça?!";
    next;
    mes "[Ninfa Sônia]";
    mes "\"Caaaaaalma " + strcharinfo(0) + "!";
    mes "Eu terminei sua couraça.\"";
    next;
    goto L_ComGrana;

L_ComGrana:
    mes "[Ninfa Sônia]";
    mes "\"Você trouxe o pagamento?\"";
    next;
    NinfaSonia_capitulo = 3;
    if (Zeny < 50000) goto L_sem_grana;
    mes "[" + strcharinfo(0) + "]";
    mes "Ok! Está aqui!";
    next;
    mes "[Ninfa Sônia]";
    mes "\"Certo, aqui está sua couraça.\"";
    getitem 3026, 1;
    Zeny = Zeny - 50000;
    NinfaSonia_capitulo = 4;
    NinfaSonia_tempo = 0;
    next;
    mes "[Ninfa Sônia]";
    mes "=] \"Foi bom fazer negócio contigo!\"";
    close;

L_sem_casulo:
    mes "[Ninfa Sônia]";
    mes "\"Deixe eu ver!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "Tome!";
    next;
    mes "[Ninfa Sônia]";
    mes "=S \"Você não tem \"Casulos de Bicho-da-Seda\" suficientes.";
    mes "Está faltando "+ (100-countitem(718)) +" casulos.\"";
    next;
    mes "[Ninfa Sônia]";
    mes "\"Volte quando tiver encontrado todos os 100!\"";
    close;

L_sem_couro:
    mes "[Ninfa Sônia]";
    mes "\"Deixe eu ver!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "Tome!";
    next;
    mes "[Ninfa Sônia]";
    mes "=S \"Você não tem \"Couros de cobra da Montanha\" suficientes.";
    mes "Está faltando " + (50 - countitem(641)) + " couros.\"";
    next;
    mes "[Ninfa Sônia]";
    mes "\"Volte quando tiver encontrado todos os 50!\"";
    close;

L_sem_grana_novamente:
    mes "[" + strcharinfo(0) + "]";
    mes "Olá Sônia!";
    next;
    mes "[Ninfa Sônia]";
    mes "\"Trouxe meu 50.000 GP de pagamento, "+ strcharinfo(0) + "?\"";
    next;
    goto L_sem_grana;

L_sem_grana:
    mes "["+ strcharinfo(0) +"]";
    mes "Eu só tenho " + Zeny + "GP, serve?";
    next;
    mes "[Ninfa Sônia]";
    mes "\"Eu não acredito, que você tem a petulância de vir pegar esta couraça, sem antes conseguir meu pagamento!\"";
    next;
    mes "[Ninfa Sônia]";
    mes "\"Vá buscar! Depois eu dou sua couraça!\"";
    close;

L_Fechar:
    close;
}
