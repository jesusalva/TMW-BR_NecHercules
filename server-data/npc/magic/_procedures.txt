function	script	magic_register	{
    //debugmes ">> Register " + .invocation$ + " @ " + strnpcinfo(0);
    .@ext$ = if_then_else(getarg(0,"") != "", "::"+getarg(0), "");
    registercmd .invocation$, strnpcinfo(0) + .@ext$; // register the spell
    .index = $@magic_index;
    $@magic_index = $@magic_index + 1;
    return;
}

-	script	Magic Timer	NPC32767,{
    end;

OnLogin:
    @_M_BLOCK = 1;
    addtimer 10000, "Magic Timer::OnClear";
    end;

OnClear:
    @_M_BLOCK = 0;
    end;
}

function	script	magic_checks	{
    .@r = 0;
    if(HIDDEN) set .@r, 1; // can not cast with @hide
    if(@_M_BLOCK) set .@r, 2; // check if last debuff ended
    if(Hp < 1) set .@r, 3; // can not cast when dead
    if (MATK1 < 1) set .@r, 4; // can not cast with a grey mana bar
    if (.@r)
        smsg SMSG_FAILURE, "Magic: Impossible to cast right now.";
    return .@r;
}

function	script	elt_damage	{
    // args are damage, dmgplus(mutation), bonus_elt, malus_elt, effect
    .@dmg = getarg(0) + rand(getarg(1));
    if(get(ELTTYPE, @target_id) == getarg(3)) // malus
        .@dmg = .@dmg / 3;
    if(get(ELTTYPE, @target_id) == getarg(2)) // bonus
        .@dmg = ((get(ELTLVL, @target_id) + 4) * .@dmg) / 4;
    .@source = .caster;
    if (!.@source) set .@source, getcharid(3);

    injure .@source, @target_id, (.@dmg * (100 - get(MDEF1, @target_id))) / 100;
    misceffect getarg(4), @target_id;
    return;
}

function	script	melee_damage	{
    // args are spell power, target id, dmg
    if ((getarg(0) - rand(100)) < (get(BaseLevel, getarg(1)) + get(MDEF1, getarg(1))))
        injure BL_ID, getarg(1), 0;
    else injure BL_ID, getarg(1), (getarg(2) * (100 - get(MDEF1, @target_id))) / 100;
    return;
}

function	script	magic_create_item	{
    // FIXME / XXX: IMO, using Luk for this is very bad and unfair
    .@exp = (MAGIC_EXPERIENCE & (BYTE_0_MASK | BYTE_1_MASK)) >> BYTE_0_SHIFT;
    .@score = (.@exp + rand(min(@spellpower, ((.@exp / 3) + 1))));
    set @create_params[2], 1; // success flag
    if (.@score >= @create_params[1]) goto L_Perfect;
    set @create_params[2], 0; // success flag
    .@score = .@score + rand(Luk) + rand(Luk);
    if (.@score < (@create_params[1] / 3)) goto L_Backfire;
    if (.@score < ((@create_params[1] * 2) / 3)) goto L_Iten;
    message strcharinfo(0), "Magic : ##3##BYour spell takes on a mind of its own!";
    if (rand(3) == 1) getitem @create_items$[1], 1; // bad item
    return;

L_Iten:
    if (rand(5) != 2) goto L_Escape;
    message strcharinfo(0), "Magic : ##3##BYour spell solidifies into the shape of a mysterious object!";
    getitem "Iten", 1;
    return;

L_Escape:
    message strcharinfo(0), "Magic : ##3##BYour spell escapes!";
    return;

L_Backfire:
    message strcharinfo(0), "Magic : ##3##BYour spell backfires!";
    if (rand(110) < Luk) heal 0 - ((BaseLevel+1)*(BaseLevel+2)*(rand(28)+3)), 0;
    else                 heal 0 - (BaseLevel + 1), 0;
    return;

L_Perfect:
    getitem @create_items$[0], @create_params[0]; // good item
    return;
}

function	script	magic_exp	{
    @last_index = (MAGIC_EXPERIENCE & BYTE_2_MASK) >> BYTE_2_SHIFT;
    @last_exp = (MAGIC_EXPERIENCE & (BYTE_0_MASK | BYTE_1_MASK)) >> BYTE_0_SHIFT;

    //debugmes "old spell index: " + @last_index;
    //debugmes "new spell index: " + .index;

    if(getskilllv(SKILL_MAGIC) < (.level + 3) && .index != @last_index)
        goto L_Gain;
    //debugmes "same as last spell => don't proceed";
    goto L_Return;

L_Gain:
    if(.exp_gain < 1) goto L_Return; // only the spells that have exp register here. If you
    //                                 remove this line then players can cast a spell with
    //                                 no cost, then a spell with a reagents, then another
    //                                 spell with no costs and still get the exp
    @new_exp = @last_exp + .exp_gain;
    if(@new_exp > (BYTE_0_MASK | BYTE_1_MASK)) set @new_exp, (BYTE_0_MASK | BYTE_1_MASK);
    //debugmes "old magic exp: "+ @last_exp;
    //debugmes "new magic exp: "+ @new_exp;
    MAGIC_EXPERIENCE = (MAGIC_EXPERIENCE &~ (BYTE_0_MASK | BYTE_1_MASK)) | (@new_exp << BYTE_0_SHIFT);
    MAGIC_EXPERIENCE = (MAGIC_EXPERIENCE &~ BYTE_2_MASK) | (.index << BYTE_2_SHIFT);
    goto L_Return;

L_Return:
    return;
}

function	script	adjust_spellpower	{
    @spellpower = MATK1 + getskilllv(SKILL_MAGIC) + getskilllv(.school) + 10;
    if((.school != SKILL_MAGIC_NATURE) && (.school != SKILL_MAGIC_LIFE)) goto L_Return;
    if(@args$ == "" || !@args$ || getpartnerid() == 0) goto L_Return;
    if(getcharid(3, @args$) < 1 || getpartnerid() != getcharid(3, @args$) || !(isloggedin(getcharid(3, @args$))))
        goto L_Return;
    //debugmes "You targeted your spouse!";
    // XXX: the spell power increases when the target is the spouse so one could
    //      just do #modrilax (spouse) right?
    //
    // ... let's just forget about spouse for now
    goto L_Return;

L_Return:
    return;
}

function	script	gain_heal_xp	{
    @last_heal_xp = ((SCRIPT_XP & SCRIPT_HEALSPELL_MASK) >>  SCRIPT_HEALSPELL_SHIFT);
    if ((@target_id != BL_ID) && ((.@heal_value / .heal_xp_value_divisor) > (((10 + @last_heal_xp) + rand(@last_heal_xp + 1)) + rand(@last_heal_xp + 1))))
        goto L_Block;
    goto L_Return;

L_Block:
    @heal_xp = (@last_heal_xp + @mexp);
    if (@heal_xp > SCRIPT_HEALSPELL_MASK)
        @heal_xp = SCRIPT_HEALSPELL_MASK;
    SCRIPT_XP = (SCRIPT_XP & ~(SCRIPT_HEALSPELL_MASK) | (@heal_xp << SCRIPT_HEALSPELL_SHIFT));
    goto L_Gain_Xp;

L_Gain_Xp:
    @heal_exp = .@heal_value;
    if (.@heal_value > get(HEALXP, @target_id))
        @heal_exp = get(HEALXP, @target_id);
    getexp (.base_exp_factor * @heal_exp), 0;
    goto L_Return;

L_Return:
    return;
}
