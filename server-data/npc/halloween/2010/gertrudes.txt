

008,91,42,0	script	Gertrudes	NPC167,3,2,{
    if (@buffStr != 0) goto L_Continuacao;

    mes "[Gertrudes]";
    mes "\"O que faz aqui se não tem forças para lutar?";
    mes "Abrigue-se em um local seguro enquanto minhas irmãs e eu fazemos o trabalho.\"";
    menu
        "Sim, você tem razão...",     L_Fechar,
        "Mas eu tenho muita força.",  L_Forte,
        "Não duvide da minha força.", L_Forte;

L_Forte:
    mes "[Gertrudes]";
    mes "\"Hahah... quanta insolência! És um sujeito de sorte, pois hoje não estou com tempo para insolências.";
    mes "Vou provar que você não sabe o que é a verdadeira força com um pouco do meu poder.\"";
    @buffStr = $@buffStr;
    next;
    goto L_Continuacao;

L_Continuacao:
    mes "[Gertrudes]";
    mes "\"Se queres força, entre no círculo mágico para que possa lhe conceder.";
    mes "Venha quantas vezes quiser... mas lembre-se de que não é minha força que irá impedir sua morte!\"";
    close;

L_Fechar:
    close;

OnTouch:
    if (@buffStr == 0 || @buffStr == $@buffStr) end;

    sc_start 37,600, 30;
    //specialeffect2 3;
    @buffStr = $@buffStr;
    end;

OnTimer20000:
    setnpctimer 0;
    $@buffStr = $@buffStr + 1;
    specialeffect 21;
    end;

OnInit:
    initnpctimer;
    $@buffStr = 1;
    end;
}
