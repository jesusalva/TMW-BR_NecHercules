


008,26,80,0	script	Alquimista	NPC542,{
    if (QUEST_AlquimistaColar == 1) goto L_Continuacao;
    if (QUEST_AlquimistaColar == 2) goto L_Concluida;

    if (Zeny > 10000 && BaseLevel < 30) goto L_Alquimista2;
    if (Zeny > 10000) goto L_Alquimista;
    goto L_frases;

L_frases:
    @n = rand(8);
    if (@n == 0) goto L_frase0;
    if (@n == 1) goto L_frase1;
    if (@n == 2) goto L_frase2;
    if (@n == 3) goto L_frase3;
    if (@n == 4) goto L_frase4;
    if (@n == 5) goto L_frase5;
    if (@n == 6) goto L_frase6;
    if (@n == 7) goto L_frase7;
    close;

L_frase0:
    mes "[Alquimista]";
    mes "\"...\"";
    close;

L_frase1:
    mes "[Alquimista]";
    mes "\"O tempo é a base de todas as coisas.\"";
    close;

L_frase2:
    mes "[Alquimista]";
    mes "\"O universo visivel possui 2,5x10^26 metros.\"";
    close;

L_frase3:
    mes "[Alquimista]";
    mes "\"Quem quer aprender a ciência, sempre são os que possuem dezenas de milhares de moedas no bolso.\"";
    close;

L_frase4:
    mes "[Alquimista]";
    mes "\"O homem e a máquina são apenas uma coisa: Números. Isso era o que Descartes acreditava.\"";
    close;

L_frase5:
    mes "[Alquimista]";
    mes "\"Contemple o universo, e por um instante poderás tocar as estrelas.\"";
    close;

L_frase6:
    mes "[Alquimista]";
    mes "\"Eu estou calculando quanta energia é liberada por todas as estrelas no universo, por favor não me interrompa.\"";
    close;

L_frase7:
    mes "[Alquimista]";
    mes "\"Quer entender o seu semelhante? Dê-lhe muito dinheiro e nada ele lhe ocultará.\"";
    close;



L_Alquimista2:
    mes "[Alquimista]";
    mes "\"Você não está preparado para entender o universo que nos cerca.";
    mes "Volte quando tiver mais experiência de vida, para que tenha maturidade para compreender as maravilhas da ciência.\"";
    close;

L_Alquimista:
    mes "[Alquimista]";
    mes "\"Vejo que tens dinheiro. O que lhe motiva vir até aqui jovem?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Vim porque preciso que alguém ilumine o meu caminho.\"";
    next;
    mes "[Alquimista]";
    mes "\"Olhe para frente, decida aonde quer chegar, e mais importante, comece a andar. Seu tesouro é sua mente, ela te mostrará para onde ir.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"E se eu duvidar de mim?\"";
    next;
    mes "[Alquimista]";
    mes "\"Se a dúvida aparecer, crie uma teoria para a resolver. Procure em um livro, eles sempre possuem as respostas as quais buscas.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"E se não encontrar as respostas que procuro?\"";
    next;
    mes "[Alquimista]";
    mes "\"Você aprenderá quem és por tentativa e erro.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Alquimista, faço estas perguntas justamente para saber quem sou eu.";
    mes "Me disseram que fui encontrado quase morto na praia e agora, a única coisa que quero saber é quem sou e de onde vim.\"";
    next;
    mes "[Alquimista]";
    mes "\"Oras, mas isso não mudará sua vida!";
    mes "Decida primeiro aonde você quer chegar, e então serás alguém.";
    mes "O seu passado já passou, contemple o futuro.";
    mes "A pequeneza de seu passado não deve ofuscar a grandeza de seu futuro!";
    mes "Você agora é semelhante a um herói, esqueça-se do que já foi e siga em frente.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Muito obrigado alquimista por compartilhar sua sabedoria. Há algo que posso fazer por você?\"";
    next;
    mes "[Alquimista]";
    mes "\"Estude muito, e serás alguém na vida. E para lhe ajudar em seu novo caminho, lhe darei um presente, se, você responder a algumas perguntas.";
    mes "O que me diz?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Seria uma honra.",                      L_Sim,
        "Acho que os ensinamentos já bastaram.", L_Nao2;

L_Nao2:
    mes "[Alquimista]";
    mes "\"Então siga calculando.\"";
    QUEST_AlquimistaColar = 2;
    goto L_Concluida;

L_Concluida:
    mes "[Alquimista]";
    mes "O alquimista parece estar muito ocupado elaborando uma nova teoria. Melhor não o incomodar.";
    if (CAP == 1 && SUBCAP == 1) goto L_iniciaMainQuest;
    close;

L_Sim:
    close2;
    mes "[Alquimista]";
    mes "\"Então, me responda: onde está teu tesouro?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Em minha força.", L_errou,
        "Em meu coração.", L_errou,
        "Em minha mente.", L_Sim2;

L_errou:
    mes "[Alquimista]";
    mes "\"Parece que você não prestou atenção ao que eu disse. Volte quando lembrar do que lhe ensinei.\"";
    QUEST_AlquimistaColar = 1;
    close;

L_Continuacao:
    mes "[Alquimista]";
    mes "\"Conseguiu entender o que te ensinei?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Sim, agora lembro de tudo.",                  L_Sim,
        "Não, ainda não consegui entender o caminho.", L_errou;

L_Sim2:
    mes "[Alquimista]";
    mes "\"Correto jovem. Lembre-se sempre disso.";
    mes "Próxima pergunta: Quando se perder no meio do caminho de tua vida, onde deves procurar luz para as suas dúvidas?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Devo procurar nos meus amigos.",         L_errou,
        "Devo procurar nos livros.",              L_Sim3,
        "Devo procurar dentro de mim mesmo.",     L_errou,
        "Devo procurar nas palavras dos sábios.", L_errou;

L_Sim3:
    mes "[Alquimista]";
    mes "\"Correto.";
    mes "Próxima pergunta: daqui em diante, o que ainda lhe resta? E com o que você se assemelha?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Me resta apenas lutar pelos meus sonhos, pois sou semelhante a uma árvore nova.",                    L_errou,
        "Me resta apenas descobrir quem sou realmente e de onde vim, pois sou semelhante a um lobo errante.", L_errou,
        "Me resta apenas meditar sobre mim mesmo, pois sou semelhante a uma pedra.",                          L_errou,
        "Me resta apenas decidir onde quero ir, pois sou semelhante a uma folha em branco.",                  L_errou,
        "Me resta apenas esquecer de tudo, e ser o mais forte, pois sou semelhante a um herói.",              L_colar;

L_colar:
    mes "[Alquimista]";
    mes "\"Sim, te darei de presente este colar, para que continues sempre assim.\"";
    getitem "ColarDeAlquimista", 1;

    QUEST_AlquimistaColar = 2;
    close;

L_iniciaMainQuest:
    // Esta quest é semelhante ao monge,
    // que dá bônus ao Herói e ao Recluso.
    PATH_GENERAL = PATH_GENERAL + 1;
    PATH_EMBAIXADOR = PATH_EMBAIXADOR + 1;

    // Define sua próxima quest para 1-2
    CAP = 1;
    SUBCAP = 2;
    getexp 300, 0;

    mes "";
    mes "[História Principal 1-2]";
    mes "Você recebeu 300 pontos de experiência.";
    mes "Você agora está no capitulo 1-2.";

    close;
}
