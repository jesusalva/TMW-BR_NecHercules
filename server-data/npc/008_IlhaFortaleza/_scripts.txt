008,22,49,0	script	#[Atenção!]#008	NPC155,{
    mes "[Atenção]";
    mes "* Esta passagem foi bloqueada! Use o desvio logo acima.";
    close;
}

008,28,42,0	script	#[Desvio!]#008	NPC155,{
    mes "[Desvio]";
    mes "* Use esta nova passagem para contornar a passagem bloqueada.";
    close;
}
