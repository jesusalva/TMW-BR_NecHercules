#!/bin/bash

export MANAPLUS="../../manaplus/src/manaplus"
if [ -f "$MANAPLUS" ]; then
    echo "Starting (local) manaplus"
else
    echo "Starting (system) manaplus"
    export MANAPLUS="manaplus"
fi

${MANAPLUS} -ud ../../client-data tmwbr.manaplus -U testesJun25 -P testesJun25
