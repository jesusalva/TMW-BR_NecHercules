#!/bin/bash

# Tmw-BR reserves a very big number of skills.
# We reserved 622 skills: 100 for class, and 22 extras.
# Now, I hope to NOT run out of skills anywhere soon...

# MAX_SKILL    1478  + 622 = 2100
# MAX_SKILL_ID 10015 + 622 + 9963 = 20622
# SC_MAX       642   + 5  = 647
# SI_MAX       966   + 5  = 971
# MAX_EVOL_SKILLS           622
# EVOL_FIRST_SKILL          20000
# OLD_MAX_SKILL_DB          1478

# can be used for custom skill id: 10016 - 10036

export VARS=" -DOLD_MAX_SKILL_DB=1478 -DMAX_SKILL=2100 -DMAX_SKILL_ID=20022 -DMAX_EVOL_SKILLS=622 -DEVOL_FIRST_SKILL=20000 -DSC_MAX=647 -DSI_MAX=971"
export CPPFLAGS="${VARS}"
